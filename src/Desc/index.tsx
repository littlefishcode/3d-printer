import React from 'react';
import styles from './index.less';
import { Badge } from 'antd';

type DescProps = {
  aim: string;
  when: string;
  tools: string;
  desc: string;
};

const Desc: React.FC<DescProps> = (props: DescProps) => {
  const { aim, when, tools, desc } = props;
  return (
    <Badge.Ribbon text="提示" color="pink">
      <div className={styles.box}>
        <div className={styles.title}>目的：</div>
        <p>{aim}</p>

        <div className={styles.title}>何时操作：</div>
        <p>{when}</p>

        <div className={styles.title}>所需工具：</div>
        <p>{tools}</p>

        <div className={styles.title}>补充说明：</div>
        <p>{desc}</p>
      </div>
    </Badge.Ribbon>
  );
};

export default Desc;
