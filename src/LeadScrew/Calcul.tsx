import React from 'react';
import { Typography } from 'antd';
const { Paragraph, Text } = Typography;

type LeadScrewProps = {
  StepSize: number; //步距角
  MicroStepping: number; //细分数
  LeadscrewPitch: number; //丝杆导程
  PulleyMotor: number; //齿轮传动比：电机
  PulleyLeadscrew: number; //齿轮传动比：丝杆
};

const calcul = (para: LeadScrewProps) => {
  return (
    (((360 / para.StepSize) * para.MicroStepping) / para.LeadscrewPitch) *
    (para.PulleyLeadscrew / para.PulleyMotor) *
    1
  ).toFixed(2);
};

const EstepCalcul: React.FC<LeadScrewProps> = (props: LeadScrewProps) => {
  const { StepSize, MicroStepping, LeadscrewPitch, PulleyMotor, PulleyLeadscrew } = props;

  return (
    <Paragraph>
      同步轮走<b>1mm</b>需要的脉冲数:<b> {calcul(props)}</b>。 理论上一个脉冲，皮带移动的距离是
      <b> {(1 / parseFloat(calcul(props))).toFixed(5)}mm</b>。
      <br />
      计算步骤：
      <b>
        (360/{props.StepSize}*{props.MicroStepping})/({props.LeadscrewPitch}
        )*({props.PulleyLeadscrew} / {props.PulleyMotor}) = {calcul(props)}
      </b>
      <br />
      <b>M92 g-code</b>可以设置移动每毫米所需的脉冲数，下面是为了设置Z轴的例子
      <pre>M92 Z{calcul(props)}</pre>
      <Text italic>
        这里不推荐使用M92来设置，因为正常情况下这个数值是3D打印机制造商写在固件中。M92一般用在装配完毕后，精调各个轴时用到。
      </Text>
    </Paragraph>
  );
};

export default EstepCalcul;
