import React from 'react';
import { Alert, Typography } from 'antd';
const { Title, Paragraph, Text, Link } = Typography;

type WarningProps = {};

const desc = () => {
  return (
    <div>
      <Paragraph>
        <ul>
          <li>
            发现Bug，可以去<Link href="https://gitee.com/littlefishcode/3d-printer/">Gitee</Link>
            报告。
          </li>
          <li>网站开源，欢迎有人接手。</li>
          <li>会逐步修改Bug。</li>
          <li>
            技术升级了， <Link href="https://seahorse.littlefish.love/">建议转到Voron</Link>{' '}
          </li>
        </ul>
      </Paragraph>
    </div>
  );
};

const Warning: React.FC<WarningProps> = (props: WarningProps) => {
  return (
    <Alert
      message="重要提示：放弃网站升级，只修复Bug，因为入坑Voron。(2023年1月27日)"
      description={desc()}
      type="warning"
      showIcon
      closable
    />
  );
};

export default Warning;
