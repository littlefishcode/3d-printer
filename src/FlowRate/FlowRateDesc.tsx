import React from 'react';
import { Typography } from 'antd';
const { Paragraph } = Typography;

type FlowRateDescProps = {
  oldFlowRate: number;
  targetThickness: number;
  measuredThickness: number;
};

const FlowRateDesc: React.FC<FlowRateDescProps> = (props: FlowRateDescProps) => {
  const { oldFlowRate, targetThickness, measuredThickness } = props;
  return (
    <div>
      <Paragraph>
        <ul>
          <li>
            首先当前的流量是 {oldFlowRate},当前目标厚度是{targetThickness}
          </li>
          <li>
            测量出{measuredThickness}mm,那么流量应该设置成多少？
            <br />
            <pre>当前目标厚度/当前测试的厚度 = a/当前的流量 这里要计算出a</pre>
          </li>
        </ul>
      </Paragraph>
    </div>
  );
};

export default FlowRateDesc;
