import React from 'react';
import { Card, Button, InputNumber, Form, Space, Tabs } from 'antd';

import FlowRateDesc from './FlowRateDesc';
import FlowRateCalcul from './FlowRateCalcul';

type FlowRateProps = {
  pOldFlowRate: number;
  pTargetThickness: number;
  pMeasuredThickness: number;
};

const layout = {
  labelCol: { span: 10 },
  wrapperCol: { span: 14 },
};
const tailLayout = {
  wrapperCol: { offset: 12, span: 12 },
};

const FlowRate: React.FC<FlowRateProps> = (props: FlowRateProps) => {
  const [form] = Form.useForm();

  const { pOldFlowRate, pTargetThickness, pMeasuredThickness } = props;

  const [oldFlowRate, setOldFlowRate] = React.useState(pOldFlowRate);
  const [targetThickness, setTargetThickness] = React.useState(pTargetThickness);
  const [measuredThickness, setMeasuredThickness] = React.useState(pMeasuredThickness);

  const onFinish = (values: {
    oldFlowRate: number;
    targetThickness: number;
    measuredThickness: number;
  }) => {
    setOldFlowRate(values.oldFlowRate);
    setTargetThickness(values.targetThickness);
    setMeasuredThickness(values.measuredThickness);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const onReset = () => {
    form.resetFields();
    setOldFlowRate(100);
    setTargetThickness(0.5);
    setMeasuredThickness(0.5);
  };

  const items = [
    {
      label: '计算结果',
      key: 'item-1',
      children: (
        <FlowRateCalcul
          oldFlowRate={oldFlowRate}
          targetThickness={targetThickness}
          measuredThickness={measuredThickness}
        />
      ),
    }, // 务必填写 key
    {
      label: '原理说明',
      key: 'item-2',
      children: (
        <FlowRateDesc
          oldFlowRate={oldFlowRate}
          targetThickness={targetThickness}
          measuredThickness={measuredThickness}
        />
      ),
    },
  ];

  return (
    <Card title="流量校正计算器" type="inner">
      <Form
        name="basic"
        {...layout}
        initialValues={{
          oldFlowRate: oldFlowRate,
          targetThickness: targetThickness,
          measuredThickness: measuredThickness,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        form={form}
      >
        <Form.Item
          label="现在设定的流量百分比"
          name="oldFlowRate"
          rules={[{ required: true, message: '请输入现在设定的流量百分比!' }]}
        >
          <InputNumber min={1} max={100} step="0.01" stringMode />
        </Form.Item>
        <Form.Item
          label="目标壁厚（mm）"
          name="targetThickness"
          rules={[{ required: true, message: '请输入目标壁厚!' }]}
        >
          <InputNumber min={0.01} max={1} step="0.01" stringMode />
        </Form.Item>

        <Form.Item
          label="实测壁厚（mm）"
          name="measuredThickness"
          rules={[{ required: true, message: '请输入实测壁厚!' }]}
        >
          <InputNumber min={0.01} max={1} step="0.01" stringMode />
        </Form.Item>

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              计算
            </Button>
            <Button htmlType="button" onClick={onReset}>
              重置
            </Button>
          </Space>
        </Form.Item>
      </Form>

      <Tabs items={items} />
    </Card>
  );
};

export default FlowRate;
