import React from 'react';
import { Typography } from 'antd';
const { Paragraph, Text } = Typography;

type FlowRateCalculProps = {
  oldFlowRate: number;
  targetThickness: number;
  measuredThickness: number;
};

const calcul = (oldFlowRate: number, targetThickness: number, measuredThickness: number) => {
  return ((oldFlowRate / measuredThickness) * targetThickness).toFixed(2);
};

const FlowRateCalcul: React.FC<FlowRateCalculProps> = (props: FlowRateCalculProps) => {
  const { oldFlowRate, targetThickness, measuredThickness } = props;

  return (
    <Paragraph>
      您的新流量应为
      <b>
        {' '}
        {calcul(oldFlowRate, targetThickness, measuredThickness)}% ，建议在切片软件中进行设置。
      </b>
      <br />
      <br />
      也可以通过M221来进行设置（不建议这么做，因为不能通过M500保存到 EEPROM,这样是一个临时设置）{' '}
      <br />
      <pre>M221 S{calcul(oldFlowRate, targetThickness, measuredThickness)}</pre>
      <br />
    </Paragraph>
  );
};

export default FlowRateCalcul;
