import React from 'react';
import { Card, Typography, InputNumber, Tabs, Row, Col, Form, Table, Image } from 'antd';
const { Paragraph, Text } = Typography;

import {
  calcPerimeter,
  calcArea,
  calcRadian,
  calcAlpha,
  calcBeta,
  calcGamma,
  calcHypotenuse,
  getDeviationAngle,
  calcOuterSides,
  myToFixed,
  showText,
} from './function';

type pProps = {
  a: number;
  b: number;
  c: number;
};

type calcDeviationProps = {
  a: number;
  b: number;
  c?: number;
};

const CalcDesc: React.FC<pProps> = (props: pProps) => {
  const { a, b, c } = props;
  return (
    <div>
      <Paragraph>
        三角型的两边分别是：a={a}、b={b} 。对角线是：c={c}，<br />
        <b>实际的夹角：{calcRadian(calcGamma(a, b, c))}</b>
        <pre>
          周长公式: P=a+b+c <br />
          <b>P={calcPerimeter(a, b, c)}</b>
          <br />
          面积公式: A=sqrt((a+b+c)*(-a+b+c)*(a-b+c)*(a+b-c))/4
          <br />
          <b>A={calcArea(a, b, c)}</b>
          <br />
          阿尔法(α)弧度: alpha = Math.acos((b * b + c * c - a * a) / (2 * b * c));
          <br />
          阿尔法(α)角度: (alpha * 180) / Math.PI； 当然也可以用Math.degress(alpha)来计算。
          <br />
          <b>
            alpha = {calcRadian(calcAlpha(a, b, c))} 角度 ({calcAlpha(a, b, c)} 弧度)
          </b>
          <br />
          贝塔弧度: beta=Math.acos((a * a + c * c - b * b) / (2 * a * c));
          <br />
          贝塔角度: (beta * 180) / Math.PI；
          <br />
          <b>
            beta = {calcRadian(calcBeta(a, b, c))} 角度 ({calcBeta(a, b, c)} 弧度)
          </b>
          <br />
          伽马弧度: gamma=acos((pow(a, 2)+pow(b, 2)-pow(c, 2))/(2*a*b))
          <br />
          伽马角度: (gamma * 180) / Math.PI；
          <br />
          <b>
            gamma = {calcRadian(calcGamma(a, b, c))} 角度 ({calcGamma(a, b, c)} 弧度)
          </b>
          <br />
        </pre>
      </Paragraph>
    </div>
  );
};

type calcDeviationDescProps = {
  angle: number;
};

/**
 * 计算偏差的原理
 * @param props
 * @returns
 */
const CalcDeviationDesc: React.FC<calcDeviationDescProps> = (props: calcDeviationDescProps) => {
  const { angle } = props;

  return (
    <div>
      <Paragraph>
        <b>预想是90°，但是发现了偏差角度，现在按照这个偏差角度，来计算偏差的距离</b>
        <br />
        <br />
        转换成一个数学问题：
        <br />
        一个直角三角型，知道斜边的长度是5，斜边与一个直角边的夹角是{getDeviationAngle(angle)}度。
        写一段js代码求两外两条直角边的长度。
        <br />
        <pre>
          const radians = (angle * Math.PI) / 180; <br />
          const sideAdjacent = hypotenuse * Math.cos(radians); <br />
          const sideOpposite = hypotenuse * Math.sin(radians); <br />
        </pre>
      </Paragraph>
    </div>
  );
};

/**
 * 结果组件
 * @param props
 * @returns
 */
const CalcResult: React.FC<pProps> = (props: pProps) => {
  const { a, b, c } = props;
  const angle = calcRadian(calcGamma(a, b, c));
  const columns = [
    {
      title: '目标',
      dataIndex: 'target',
      key: 'target',
    },
    {
      title: '实际',
      dataIndex: 'actual',
      key: 'actual',
      render: (value: number) => myToFixed(value),
    },
    {
      title: '纵向偏差',
      dataIndex: 'deviationX',
      key: 'deviationX',
      render: (value: number) => myToFixed(value),
    },
    {
      title: '横向偏差',
      dataIndex: 'deviationY',
      key: 'deviationY',
      render: (value: number) => myToFixed(value),
    },
  ];

  type dataSourceProps = {
    target: number;
    actual: number;
    deviationX: number;
    deviationY: number;
  };
  function getDataSource(deviationAngle: number) {
    let array: number[] = [5, 20, 100, 200, 300];
    let ren: dataSourceProps[] = array.map((n) => {
      let sizes = calcOuterSides(n, deviationAngle);
      return {
        target: n,
        actual: sizes.sideAdjacent,
        deviationX: n - sizes.sideAdjacent,
        deviationY: sizes.sideOpposite,
      };
    });

    return ren;
  }

  return (
    <div>
      <Paragraph>
        <b>实际的夹角：{myToFixed(angle)}。</b> <Text type="danger">{showText(angle)}</Text>
        <br />
        <b>偏差角度=90-实际角度={myToFixed(getDeviationAngle(angle))}</b>
      </Paragraph>
      <Table
        dataSource={getDataSource(getDeviationAngle(angle))}
        columns={columns}
        size="small"
        pagination={false}
      />
    </div>
  );
};

/**
 * 最外层组件
 * @param props
 * @returns
 */
const CalcDeviation: React.FC<calcDeviationProps> = (props: calcDeviationProps) => {
  const [a, setA] = React.useState(props.a);
  const [b, setB] = React.useState(props.b);
  const [c, setC] = React.useState(props.c ? props.c : calcHypotenuse(a, b));

  const layout = {
    labelCol: { span: 10 },
    wrapperCol: { span: 14 },
  };

  const items = [
    {
      label: '结果',
      key: 'step-2-1',
      children: <CalcResult a={a} b={b} c={c} />,
    }, // 务必填写 key
    {
      label: '求三角形内角',
      key: 'step-2-2',
      children: <CalcDesc a={a} b={b} c={c} />,
    }, // 务必填写 key

    {
      label: '计算偏差',
      key: 'step-2-3',
      children: <CalcDeviationDesc angle={calcRadian(calcGamma(a, b, c))} />,
    }, // 务必填写 key
  ];

  return (
    <Card title="偏差评估工具" type="inner">
      按照正常情况，伽马角=90°，因为装歪了，不是直角，如下图：
      <br />
      <Image width={200} src="/sj.jpg" />
      <Form name="basic" {...layout} initialValues={{ a, b, c }} autoComplete="off">
        <Row>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item
              label="实际测量的c长度 "
              name="c"
              rules={[{ required: true, message: '请输入对角线!' }]}
            >
              <InputNumber
                style={{ width: 200 }}
                min={1}
                max={1000}
                step="0.00000000000001"
                stringMode
                onChange={(value) => setC(value ? value : 1)}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item label="a" name="a" rules={[{ required: true, message: '请输入A!' }]}>
              <InputNumber
                min={1}
                max={1000}
                step="1"
                onChange={(value) => setA(value ? value : 1)}
              />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item label="b" name="b" rules={[{ required: true, message: '请输入B!' }]}>
              <InputNumber
                min={1}
                max={1000}
                step="1"
                onChange={(value) => setB(value ? value : 1)}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <Tabs items={items} />{' '}
    </Card>
  );
};

export default CalcDeviation;
