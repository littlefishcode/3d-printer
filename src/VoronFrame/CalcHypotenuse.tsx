import React from 'react';
import { Card, Typography, InputNumber, Tabs, Row, Col, Form } from 'antd';
const { Paragraph } = Typography;

import { calcHypotenuse } from './function';

type pProps = {
  a: number;
  b: number;
};

const CalcResult: React.FC<pProps> = (props: pProps) => {
  const { a, b } = props;
  return (
    <div>
      <Paragraph>
        斜边长度：<b>{calcHypotenuse(a, b)}</b>
        <p>知道斜边的长度了，测量的时候，心里就有数了。</p>
      </Paragraph>
    </div>
  );
};

const CalcDesc: React.FC<pProps> = (props: pProps) => {
  const { a, b } = props;

  return (
    <div>
      <Paragraph>
        直角三角型的两边分别是：{a}、{b}，对角线长度是<b>{calcHypotenuse(a, b)}</b>
        <pre>
          根据勾股定律，两个直角边的平方和等于斜边的平方
          <br />
          对角线= Math.sqrt({a} * {a} + {b} * {b})
          <br />
          Math.sqrt 是开平方函数
        </pre>
      </Paragraph>
    </div>
  );
};

const CalcHypotenuse: React.FC<pProps> = (props: pProps) => {
  const [a, setA] = React.useState(props.a);
  const [b, setB] = React.useState(props.b);

  const layout = {
    labelCol: { span: 10 },
    wrapperCol: { span: 14 },
  };

  const items = [
    {
      label: '结果',
      key: 'step-0',
      children: <CalcResult a={a} b={b} />,
    }, // 务必填写 key
    {
      label: '数学原理',
      key: 'step-1',
      children: <CalcDesc a={a} b={b} />,
    }, // 务必填写 key
  ];

  return (
    <Card title="直角三角形斜边计算工具" type="inner">
      <Form name="basic" {...layout} initialValues={props} autoComplete="off">
        <Row>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item label="a长度(mm)" name="a" rules={[{ required: true, message: '请输入A!' }]}>
              <InputNumber
                min={1}
                max={1000}
                step="1"
                onChange={(value) => setA(value ? value : 1)}
              />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item label="b长度(mm)" name="b" rules={[{ required: true, message: '请输入B!' }]}>
              <InputNumber
                min={1}
                max={1000}
                step="1"
                onChange={(value) => setB(value ? value : 1)}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
      <Tabs items={items} />{' '}
    </Card>
  );
};

export default CalcHypotenuse;
