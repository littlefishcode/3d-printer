/**
 * 对角线的长度
 * @param y
 * @param x
 * @returns
 */
export function calcHypotenuse(y: number, x: number) {
  return Math.sqrt(y * y + x * x);
}

/**
 * 周长
 * @param ap
 * @param bp
 * @param cp
 * @returns
 */
export function calcPerimeter(ap: number, bp: number, cp: number) {
  return ap + bp + cp;
}

/**
 * 面积
 * @param ap
 * @param bp
 * @param cp
 * @returns
 */
export function calcArea(ap: number, bp: number, cp: number) {
  return Math.sqrt((ap + bp + cp) * (-ap + bp + cp) * (ap - bp + cp) * (ap + bp - cp)) / 4;
}

/**
 * 根据弧度求角度
 * @param acos
 * @returns
 */
export function calcRadian(acos: number) {
  return (acos * 180) / Math.PI;
}

/**
 * 阿尔法(α)弧度
 * @param a
 * @param b
 * @param c
 * @returns
 */
export function calcAlpha(a: number, b: number, c: number) {
  return Math.acos((b * b + c * c - a * a) / (2 * b * c));
}

/**
 * 贝塔弧度
 * @param a
 * @param b
 * @param c
 * @returns
 */
export function calcBeta(a: number, b: number, c: number) {
  return Math.acos((a * a + c * c - b * b) / (2 * a * c));
}

/**
 * 伽马弧度
 * @param a
 * @param b
 * @param c
 * @returns
 */
export function calcGamma(a: number, b: number, c: number) {
  return Math.acos((a * a + b * b - c * c) / (2 * a * b));
}

/**
 * 直角三角形，知道斜边长度与直角的夹角，计算出两条边的长度
 * @param hypotenuse
 * @param angle
 * @returns
 */

export function calcOuterSides(hypotenuse: number, angle: number) {
  // Convert the angle from degrees to radians
  const radians = (angle * Math.PI) / 180;

  // Calculate the length of the side adjacent to the angle
  const sideAdjacent = hypotenuse * Math.cos(radians);

  // Calculate the length of the side opposite to the angle
  const sideOpposite = hypotenuse * Math.sin(radians);

  // Return the lengths of the sides as an object
  return {
    sideAdjacent: sideAdjacent,
    sideOpposite: sideOpposite,
  };
}

/**
 * 计算出与90°角度的偏差角度。
 * @param angle
 * @returns
 */
export function getDeviationAngle(angle: number) {
  if (90 > angle) {
    return 90 - angle;
  } else {
    return angle - 90;
  }
}

export function myToFixed(a: number) {
  return a.toFixed(6);
}

export function showText(angle: number) {
  let ok = 0.001;
  if (angle < 90 + ok && angle > 90 - ok) {
    return '在偏差范围';
  }

  if (angle >= 90 + ok) {
    return '向外偏';
  }

  if (angle <= 90 - ok) {
    return '向内偏';
  }
}
