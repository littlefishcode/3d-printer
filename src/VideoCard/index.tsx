import React from 'react';
import { Col, Row } from 'antd';

import styles from './index.less';

type VideoCardProps = {
  children?: React.ReactNode;
};

/**
 * 控件内容
 * @param props
 * @returns
 */
const VideoCard: React.FC<VideoCardProps> = (props: VideoCardProps) => {
  const { children } = props;
  return (
    <Row>
      <Col xs={24} sm={24} md={24} lg={24} xl={14}>
        <div className={styles.card}>{children}</div>
      </Col>

      <Col xs={24} sm={24} md={24} lg={24} xl={10}></Col>
    </Row>
  );
};

export default VideoCard;
