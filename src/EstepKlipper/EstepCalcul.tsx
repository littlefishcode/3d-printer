import React from 'react';
import { Typography } from 'antd';
const { Paragraph, Text } = Typography;

type EstepCalculProps = {
  oldEstep: number;
  measurement: number;
};

const calcul = (oldEstep: number, measurement: number) => {
  return ((100 * oldEstep) / (120 - measurement)).toFixed(4);
};

const EstepCalcul: React.FC<EstepCalculProps> = (props: EstepCalculProps) => {
  const { oldEstep, measurement } = props;

  return (
    <Paragraph>
      剩余<b> {measurement}mm </b>耗材，这意味着您挤出了<b> {120 - measurement}mm </b>。您的新
      E-steps 应为<b> {calcul(oldEstep, measurement)} </b>在config.cfg中输入以下内容：
      <br />
      <pre>rotation_distance: {calcul(oldEstep, measurement)}</pre>
      保存完毕后重启
      <br />
    </Paragraph>
  );
};

export default EstepCalcul;
