import React from 'react';
import { Card, Button, InputNumber, Form, Space, Tabs } from 'antd';

import EstepDesc from './EstepDesc';
import EstepCalcul from './EstepCalcul';

type EstepProps = {
  pMeasurement: number;
  pOldEstep: number;
};

const layout = {
  labelCol: { span: 10 },
  wrapperCol: { span: 14 },
};
const tailLayout = {
  wrapperCol: { offset: 12, span: 12 },
};

const Estep: React.FC<EstepProps> = (props: EstepProps) => {
  const [form] = Form.useForm();

  const { pMeasurement, pOldEstep } = props;

  const [oldEstep, setOldEstep] = React.useState(pOldEstep);
  const [measurement, setMeasurement] = React.useState(pMeasurement);

  const onFinish = (values: { oldEstep: number; measurement: number }) => {
    setOldEstep(values.oldEstep);
    setMeasurement(values.measurement);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const onReset = () => {
    form.resetFields();
    setOldEstep(22.6789511);
    setMeasurement(20);
  };

  const items = [
    {
      label: '计算结果',
      key: 'item-1',
      children: <EstepCalcul measurement={measurement} oldEstep={oldEstep} />,
    }, // 务必填写 key
    {
      label: '原理说明',
      key: 'item-2',
      children: <EstepDesc measurement={measurement} oldEstep={oldEstep} />,
    },
  ];

  return (
    <Card title="挤出机校正计算器" type="inner">
      <Form
        name="basic"
        {...layout}
        initialValues={{ oldEstep: oldEstep, measurement: measurement }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        form={form}
      >
        <Form.Item
          label="现在的rotation_distance"
          name="oldEstep"
          rules={[{ required: true, message: '请输入现在的rotation_distance!' }]}
        >
          <InputNumber min={1} max={100} step="0.001" stringMode />
        </Form.Item>
        <Form.Item
          label="当前挤出机入口与标记测量值(mm)"
          name="measurement"
          rules={[{ required: true, message: '请输入测量的距离!' }]}
        >
          <InputNumber min={1} max={100} step="0.001" stringMode />
        </Form.Item>

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              计算
            </Button>
            <Button htmlType="button" onClick={onReset}>
              重置
            </Button>
          </Space>
        </Form.Item>
      </Form>

      <Tabs items={items} />
    </Card>
  );
};

export default Estep;
