import React from 'react';
import { Card, InputNumber, Tabs, message } from 'antd';

import styles from './index.less';

import XyzDesc from './XyzDesc';

type XyzStepProps = {
  xDefaultValue: number;
  yDefaultValue: number;
  zDefaultValue: number;
  xExpectValue: number;
  yExpectValue: number;
  zExpectValue: number;
  xActualValue: number;
  yActualValue: number;
  zActualValue: number;
};

/**
 * 控件内容
 * @param props
 * @returns
 */
const XyzStep: React.FC<XyzStepProps> = (props: XyzStepProps) => {
  const {
    xDefaultValue,
    yDefaultValue,
    zDefaultValue,
    xExpectValue,
    yExpectValue,
    zExpectValue,
    xActualValue,
    yActualValue,
    zActualValue,
  } = props;

  const [xd, setXd] = React.useState(xDefaultValue);
  const [yd, setYd] = React.useState(yDefaultValue);
  const [zd, setZd] = React.useState(zDefaultValue);

  const [xe, setXe] = React.useState(xExpectValue);
  const [ye, setYe] = React.useState(yExpectValue);
  const [ze, setZe] = React.useState(zExpectValue);

  const [xa, setXa] = React.useState(xActualValue);
  const [ya, setYa] = React.useState(yActualValue);
  const [za, setZa] = React.useState(zActualValue);

  const getResult = (d: number, e: number, a: number): string => {
    return ((d / a) * e).toFixed(2);
  };

  const items = [
    {
      label: '计算结果',
      key: 'item-1',
      children: (
        <XyzDesc xr={getResult(xd, xe, xa)} yr={getResult(yd, ye, ya)} zr={getResult(zd, ze, za)} />
      ),
    }, // 务必填写 key
  ];

  return (
    <Card title="XYZ轴精度调整" type="inner">
      <table className={styles.customers}>
        <tr>
          <th>轴</th>
          <th>原步进值</th>
          <th>预计长度(mm)</th>
          <th>实际长度(mm)</th>
          <th>新步进值</th>
        </tr>
        <tr>
          <td>X</td>
          <td>
            <InputNumber
              min={1}
              defaultValue={xDefaultValue}
              key="xDefaultValue"
              step="0.01"
              stringMode
              onChange={(value) => (value ? setXd(value) : setXd(0))}
            />
          </td>
          <td>
            <InputNumber
              min={1}
              defaultValue={xExpectValue}
              key="xExpectValue"
              step="0.01"
              stringMode
              onChange={(value) => (value ? setXe(value) : setXe(0))}
            />
          </td>
          <td>
            <InputNumber
              min={1}
              defaultValue={xActualValue}
              key="xActualValue"
              step="0.01"
              stringMode
              onChange={(value) => (value ? setXa(value) : setXa(0))}
            />
          </td>
          <td>{getResult(xd, xe, xa)}</td>
        </tr>
        <tr>
          <td>Y</td>
          <td>
            <InputNumber
              min={1}
              defaultValue={xDefaultValue}
              key="yDefaultValue"
              step="0.01"
              stringMode
              onChange={(value) => (value ? setYd(value) : setYd(0))}
            />
          </td>
          <td>
            <InputNumber
              min={1}
              defaultValue={yExpectValue}
              key="yExpectValue"
              step="0.01"
              stringMode
              onChange={(value) => (value ? setYe(value) : setYe(0))}
            />
          </td>
          <td>
            <InputNumber
              min={1}
              defaultValue={yActualValue}
              key="yActualValue"
              step="0.01"
              stringMode
              onChange={(value) => (value ? setYa(value) : setYa(0))}
            />
          </td>
          <td>{getResult(yd, ye, ya)}</td>
        </tr>
        <tr>
          <td>Z</td>
          <td>
            <InputNumber
              min={1}
              defaultValue={zDefaultValue}
              key="zDefaultValue"
              step="0.01"
              stringMode
              onChange={(value) => (value ? setZd(value) : setZd(0))}
            />
          </td>
          <td>
            <InputNumber
              min={1}
              defaultValue={zExpectValue}
              key="zExpectValue"
              step="0.01"
              stringMode
              onChange={(value) => (value ? setZe(value) : setZe(0))}
            />
          </td>
          <td>
            <InputNumber
              min={1}
              defaultValue={zActualValue}
              key="zActualValue"
              step="0.01"
              stringMode
              onChange={(value) => (value ? setZa(value) : setZa(0))}
            />
          </td>
          <td>{getResult(zd, ze, za)}</td>
        </tr>
      </table>
      <br />
      z轴丝杆导程设置标准：8-400 ; 4-800 ; 2-1600 ;
      <br />
      <br />
      <Tabs items={items} />
    </Card>
  );
};

export default XyzStep;
