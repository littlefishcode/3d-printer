import React from 'react';
import { Typography } from 'antd';
const { Paragraph } = Typography;

type XyzDescProps = {
  xr: string;
  yr: string;
  zr: string;
};

const XyzDesc: React.FC<XyzDescProps> = (props: XyzDescProps) => {
  const { xr, yr, zr } = props;
  return (
    <div>
      <Paragraph>
        输入下面的命令，就可以保存新的设置了。
        <pre>
          M92 X{xr} Y{yr} Z{zr}
          <br />
          M500
        </pre>
        也可以把上面的内容，放到一个新的gcode文件中，复制到打印机，然后在打印机上打印这个文件就可以了。
      </Paragraph>
    </div>
  );
};

export default XyzDesc;
