import React from 'react';
import { Card, InputNumber, Form, Tabs, Select, Row, Col } from 'antd';

import Calcul from './Calcul';

type ExtruderProps = {
  StepSize: number; //步距角
  MicroStepping: number; //细分数
  Diameter: number; //挤出机齿轮直径
};

const StepSizeOptions = [
  {
    value: '1.8',
    label: '1.8°-每圈转200次 42步进电机',
  },
  {
    value: '0.9',
    label: '0.9°-每圈转400次',
  },
  {
    value: '7.5',
    label: '7.5°-每圈转48次',
  },
];

const MicroSteppingOptions = [
  {
    value: '1',
    label: '1-full step',
  },
  {
    value: '2',
    label: '1/2-half step',
  },
  {
    value: '4',
    label: '1/4-quater step',
  },
  {
    value: '16',
    label: '1/16-A4988',
  },

  {
    value: '32',
    label: '1/32-DRV8825',
  },

  {
    value: '256',
    label: '1/256-TMC2209',
  },
];

const layout = {
  labelCol: { span: 10 },
  wrapperCol: { span: 14 },
};
const tailLayout = {
  wrapperCol: { offset: 12, span: 12 },
};

const Extruder: React.FC<ExtruderProps> = (props: ExtruderProps) => {
  const [form] = Form.useForm();
  const [StepSize, setStepSize] = React.useState(props.StepSize);
  const [MicroStepping, setMicroStepping] = React.useState(props.MicroStepping);
  const [Diameter, setDiameter] = React.useState(props.Diameter);

  const items = [
    {
      label: '计算结果',
      key: 'item-1',
      children: <Calcul StepSize={StepSize} MicroStepping={MicroStepping} Diameter={Diameter} />,
    }, // 务必填写 key
  ];

  return (
    <Card title="移动1mm所需脉冲数" type="inner">
      <Form name="basic" {...layout} initialValues={props} autoComplete="off" form={form}>
        <Row>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item label="电机步距角" name="StepSize">
              <Select options={StepSizeOptions} onChange={(value) => setStepSize(value)} />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item label="驱动细分数" name="MicroStepping">
              <Select
                options={MicroSteppingOptions}
                onChange={(value) => setMicroStepping(value)}
              />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item
              label="挤出机齿轮直径"
              name="Diameter"
              rules={[{ required: true, message: '请输入挤出机齿轮直径!' }]}
            >
              <InputNumber
                min={1}
                max={100}
                step="1"
                onChange={(value) => setDiameter(value ? value : 1)}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>

      <Tabs items={items} />
    </Card>
  );
};

export default Extruder;
