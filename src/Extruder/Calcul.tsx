import React from 'react';
import { Typography } from 'antd';
const { Paragraph, Text } = Typography;

type ExtruderProps = {
  StepSize: number; //步距角
  MicroStepping: number; //细分数
  Diameter: number; //挤出机齿轮直径
};

const calcul = (para: ExtruderProps) => {
  return ((((360 / para.StepSize) * para.MicroStepping) / (para.Diameter * 3.1415926)) * 1).toFixed(
    2,
  );
};

const EstepCalcul: React.FC<ExtruderProps> = (props: ExtruderProps) => {
  const { StepSize, MicroStepping, Diameter } = props;

  return (
    <Paragraph>
      挤出机耗材走<b>1mm</b>需要的脉冲数:<b> {calcul(props)}</b>。 理论上一个脉冲，挤出的耗材是
      <b> {(1 / parseFloat(calcul(props))).toFixed(5)}mm</b>。
      <br />
      计算步骤：
      <b>
        (360/{props.StepSize}*{props.MicroStepping})/({props.Diameter}*3.1415926 ) = {calcul(props)}
      </b>
      <br />
      <b>M92 g-code</b>可以设置移动每毫米所需的脉冲数，下面是为了设置Z轴的例子
      <pre>M92 E{calcul(props)}</pre>
      <Text italic>
        这里不推荐使用M92来设置，因为正常情况下这个数值是3D打印机制造商写在固件中。M92一般用在装配完毕后，精调各个轴时用到。
      </Text>
    </Paragraph>
  );
};

export default EstepCalcul;
