// ---------------回抽塔设置-------------------------------
export declare type RectractTowerSettings = {
  command: 'speed' | 'distance';
  startValue: number; //The starting value of the Tower
  valueChange: number; //The value change of each block, can be positive or negative
  changelayer: number; //How many layers needs to be printed before the value should
  changelayeroffset: number; //If the print has a base, indicate the number of layers from
  lcdfeedback: boolean; //This setting will insert M117 gcode instructions, to display current modification in the G-Code is being used.
};

/**
 * 是不是layer开始行
 * @param line Gcode line
 * @returns bool: True if the line is the start of a layer section
 */
export const is_begin_layer_line = (line: string): boolean => {
  return line.startsWith(';LAYER:');
};

/**
 * 检测当前行是不是回抽的行，只包含G1 F E，没有 X Y Z
 * @param line Gcode line
 * @returns bool: True if the line is a retract segment
 */
export const is_retract_line = (line: string): boolean => {
  return (
    line.includes('G1') &&
    line.includes('F') &&
    line.includes('E') &&
    !line.includes('X') &&
    !line.includes('Y') &&
    !line.includes('Z')
  );
};

/**
 * Check if current line is a standard printing segment
 * 检测当前行是不是一个标准的输出行
 * @param line
 * @returns  True if the line is a standard printing segment
 */
export const is_extrusion_line = (line: string): boolean => {
  return line.includes('G1') && line.includes('X') && line.includes('Y') && line.includes('E');
};

/**
 * 检查当前行是不是快速移动动行
 * Check if current line is a rapid movement segment
 * @param line
 * @returns
 */
export const is_not_extrusion_line = (line: string): boolean => {
  return line.includes('G0') && line.includes('X') && line.includes('Y') && !line.includes('E');
};

/**
 * 是否是相对定位
 * Check if current line contain a M83 / G91 instruction
 * M83 - 将 E 设置为相对定位
 * 此命令用于覆盖G90E 轴并将其置于独立于其他轴的相对模式。
 * G90 - 绝对定位
 * G91 - 相对定位 设置相对位置模式。在这种模式下，所有坐标都被解释为相对于最后一个位置。这包括挤出机位置，除非被M82.
 * @param line Gcode line
 * @returns True contain a M83 / G91 instruction
 */
export const is_relative_instruction_line = (line: string): boolean => {
  return line.includes('G91') || line.includes('M83');
};

/**
 * 是否是绝对位置
 * Check if current line contain a M82 / G90 instruction
 * G90 - 绝对定位
 * M82 - 将 E 设置为绝对定位  在cure中默认是这个定位
 * @param line
 * @returns
 */
export const is_not_relative_instruction_line = (line: string): boolean => {
  return line.includes('G90') || line.includes('M82');
};

/**
 * 设置每单位的轴步数, 这个一般在;LAYER:0之前设定
 * Check if current line contain a G92 E0
 * @param line Gcode line
 * @returns True contain a G92 E0 instruction
 */
export const is_reset_extruder_line = (line: string): boolean => {
  return line.includes('G92') && line.includes('E0');
};

/**
 * todo 通过文件头，获取当前第一个挤出机的relative_extrusion
 * M82 ;absolute extrusion mode cura默认都是absolute定位
 * @param data  文件的头部定义部分。
 * @returns 是否是相对定位，cura默认都是absolute定位.
 */
export const is_relative_extrusion = (data: string): boolean => {
  //return data.includes('M83 ;') ;
  return false;
};

export declare type Header = {
  layerCount: number; //层的数量，例如：;LAYER_COUNT:157
  layerHeight: number; //层高，例如：;Layer height: 0.25
  generator: 'cura' | 'undefine'; //那个工具产生的，例如：;Generated with Cura
  extrusionMode: 'absolute' | 'relative';
};

export const getHeader = (data: string): Header => {
  const header: Header = {
    layerCount: 0,
    layerHeight: 0.25,
    generator: 'cura',
    extrusionMode: 'absolute',
  };

  header.extrusionMode = data.includes('M82 ;') ? 'absolute' : 'relative';
  header.generator = data.includes(';Generated with Cura') ? 'cura' : 'undefine';

  //得到层数：;LAYER_COUNT:157
  var r = /;LAYER_COUNT:\d+/g;
  var result = data.match(r);
  if (result) {
    header.layerCount = Number(result[0].substring(result[0].lastIndexOf(':') + 1));
  }

  //得到层高  ;Layer height: 0.25
  r = /;Layer height: [0-9]+(\.[0-9]*)*/g;
  var result = data.match(r);
  if (result) {
    header.layerHeight = Number(result[0].substring(result[0].lastIndexOf(':') + 1));
  }

  return header;
};

export const retractTowerExec = (self: RectractTowerSettings, data: string): string => {
  const header = getHeader(data);
  //得到挤出机是否是相对模式，cura默认是绝对模式。
  var relative_extrusion: boolean = header.extrusionMode === 'relative';

  //得到用户的设置内容
  const UseLcd = self.lcdfeedback;
  const Instruction = self.command;
  const StartValue = self.startValue;
  const ValueChange = self.valueChange;
  const ChangeLayer = self.changelayer;

  //这里有点奇怪，底座层数：为啥偏移要+2呀? 实际上layer是从0开始的，例如底座有3层：0 1 2,真正的层数从3也就是第4层开始
  const ChangeLayerOffset = self.changelayeroffset + 2;

  var CurrentValue = -1;
  var save_e = -1;
  var Command = '';

  return data;
};
