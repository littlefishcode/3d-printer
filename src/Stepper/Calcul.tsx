import React from 'react';
import { Typography } from 'antd';
const { Paragraph, Text } = Typography;

type StepperProps = {
  StepSize: number; //步距角
  MicroStepping: number; //细分数
  BeltPitch: number; //皮带间距
  ToothCount: number; //滑轮齿数
};

const calcul = (para: StepperProps) => {
  return (
    (((360 / para.StepSize) * para.MicroStepping) / (para.ToothCount * para.BeltPitch)) *
    1
  ).toFixed(2);
};

const EstepCalcul: React.FC<StepperProps> = (props: StepperProps) => {
  const { StepSize, MicroStepping, BeltPitch, ToothCount } = props;

  return (
    <Paragraph>
      同步轮走<b>1mm</b>需要的脉冲数:<b> {calcul(props)}</b>。 理论上一个脉冲，皮带移动的距离是
      <b> {(1 / parseFloat(calcul(props))).toFixed(5)}mm</b>。
      <br />
      计算步骤：
      <b>
        (360/{props.StepSize}*{props.MicroStepping})/{props.ToothCount}*{props.BeltPitch} ={' '}
        {calcul(props)}
      </b>
      <br />
      <b>M92 g-code</b>可以设置移动每毫米所需的脉冲数，下面是为了设置X轴的例子
      <pre>M92 X{calcul(props)}</pre>
      <Text italic>
        这里不推荐使用M92来设置，因为正常情况下这个数值是3D打印机制造商写在固件中。M92一般用在装配完毕后，精调各个轴时用到。
      </Text>
    </Paragraph>
  );
};

export default EstepCalcul;
