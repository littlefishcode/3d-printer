import React from 'react';
import { DownloadOutlined } from '@ant-design/icons';
import { Button } from 'antd';

type DownFileProps = {
  downLoadFile: () => void;
};

const DownFile: React.FC<DownFileProps> = (props: DownFileProps) => {
  const { downLoadFile } = props;
  return (
    <>
      <Button type="primary" icon={<DownloadOutlined />} onClick={() => downLoadFile()}>
        Download
      </Button>
    </>
  );
};

export default DownFile;
