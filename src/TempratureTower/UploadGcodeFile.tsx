import React from 'react';
import { UploadOutlined } from '@ant-design/icons';
import { Button, message, Upload } from 'antd';
import { RcFile, UploadChangeParam, UploadFile } from 'antd/lib/upload/interface';
import type { UploadProps } from 'antd';

type UploadGcodeFileProps = {};

// 向外暴露的方法
export type refProps = {
  getFile: () => RcFile | null;
};

const UploadGcodeFile = React.forwardRef<refProps, UploadGcodeFileProps>(
  (props: UploadGcodeFileProps, ref) => {
    // 保存在ref中，这样比usestate好，因为usestate会刷新页面
    const refData = React.useRef<RcFile | null>(null);

    // 定义向外暴漏的方法
    React.useImperativeHandle(ref, () => ({
      getFile: () => {
        return refData.current;
      },
    }));

    const uploadProps: UploadProps = {
      name: 'file',
      action: '#',
      headers: {
        authorization: 'authorization-text',
      },
      maxCount: 1,
      showUploadList: {
        showRemoveIcon: false,
      },

      onChange(info: UploadChangeParam<UploadFile>) {
        if (info.file.status !== 'uploading') {
          // console.log(info.file, info.fileList);
        }

        if (info.file.status === 'done') {
          message.success(`${info.file.name} 上传成功`);
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} 上传失败`);
        }
      },

      beforeUpload(file: RcFile) {
        const isGCODE = file.name.substring(file.name.lastIndexOf('.')) === '.gcode';
        if (!isGCODE) {
          message.error(`${file.name} 不是一个gcode文件`);
          return isGCODE || Upload.LIST_IGNORE;
        }
        if (file !== null) {
          // 获取当前的文件，并传递给父亲窗口。
          refData.current = file;
        }
        return isGCODE || Upload.LIST_IGNORE;
      },
    };

    return (
      <Upload {...uploadProps}>
        <Button icon={<UploadOutlined />}>上传</Button>
      </Upload>
    );
  },
);

export default UploadGcodeFile;
