import React from 'react';
import { Col, Row, Image, InputNumber, Typography, Checkbox } from 'antd';

const { Paragraph, Text } = Typography;

import styles from './TempratureTable.less';

import { getTempratureTowerDate, TempratureTowerDataProps } from './Data';

type TempratureTableProps = {};
// 向外暴露的方法
export type refProps = {
  getTempratureTowerData: () => TempratureTowerDataProps[];
};

const TempratureTable = React.forwardRef<refProps, TempratureTableProps>(
  (props: TempratureTableProps, ref) => {
    // 保存在ref中，这样比usestate好，因为usestate会刷新页面
    const data = React.useRef<TempratureTowerDataProps[]>(getTempratureTowerDate());
    const [layerNoDisabled, setLayerNoDisabled] = React.useState(true);

    // 定义向外暴漏的方法
    React.useImperativeHandle(ref, () => ({
      getTempratureTowerData: () => {
        return data.current;
      },
    }));

    const onChangeTemprature = (value: number | null, index: number) => {
      if (value !== null) {
        data.current[index].temprature = value;
      }
    };

    const onChangeLayerNo = (value: number | null, index: number) => {
      if (value !== null) {
        data.current[index].layerNo = value;
      }
    };

    const onChangeCheckBox = (e: any) => {
      setLayerNoDisabled(!e.target.checked);
    };

    return (
      <div>
        <Row>
          <Col xl={12} sm={24}>
            <table>
              <thead>
                <tr>
                  <th className={styles.th}>参考模型</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <Image
                      src="/temperaturediagram.jpg"
                      preview={false}
                      style={{ marginTop: -6 }}
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </Col>
          <Col xl={12} sm={24}>
            <table className={styles.table}>
              <thead>
                <tr className={styles.textAlign}>
                  <th style={{ width: '30%' }} className={styles.th}>
                    部分
                  </th>
                  <th className={styles.th}>温度</th>
                  <th className={styles.th}>开始层编号</th>
                </tr>
              </thead>
              <tbody>
                {data.current.map((item, index) => (
                  <tr className={styles.textAlign}>
                    <td>{item.lable}&nbsp;&nbsp;</td>
                    <td>
                      <InputNumber
                        min={180}
                        max={280}
                        defaultValue={item.temprature}
                        onChange={(value) => {
                          onChangeTemprature(value, index);
                        }}
                        key={5000 + item.layerNo}
                      />
                    </td>
                    <td>
                      <InputNumber
                        min={1}
                        max={2280}
                        defaultValue={item.layerNo}
                        onChange={(value) => {
                          onChangeLayerNo(value, index);
                        }}
                        disabled={layerNoDisabled}
                        key={item.layerNo}
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </Col>
        </Row>
        <Paragraph>
          <blockquote>
            如果是0.5mm喷嘴、0.25mm层高进行切片，就不建议修改层编号，因为默认的就是这种切片的数据，A从第4层开始，每32层后，进入下一部分测试。
            <br />
            <Checkbox onChange={onChangeCheckBox}>我想修改层编号</Checkbox>
          </blockquote>
        </Paragraph>
      </div>
    );
  },
);

export default TempratureTable;
