// ---------------温度塔结构-------------------------------
export declare type TempratureTowerDataProps = {
  lable: string;
  temprature: number;
  layerNo: number;
};

// 根据这个规则，可以自动生成数据
export declare type TempratureTowerRule = {
  firstLayerTemprature: number;
  SegmentCount: number; //总的部分数
  firstSegmentLayerNo: number; //第一个开始的部分的层编号
  firstSegmentTemprature: number;
  addTemprature: number; //  递进的温度
  addLayerNo: number; // 递进的层数
  lableGenerator: string; // 生成器，按照温度或者A B C D排序
};

const tempratureTowerData: TempratureTowerDataProps[] = [
  {
    lable: 'E',
    temprature: 210,
    layerNo: 131,
  },
  {
    lable: 'D',
    temprature: 205,
    layerNo: 100,
  },
  {
    lable: 'C',
    temprature: 200,
    layerNo: 68,
  },
  {
    lable: 'B',
    temprature: 195,
    layerNo: 36,
  },
  {
    lable: 'A',
    temprature: 190,
    layerNo: 4,
  },
  {
    lable: '第一层',
    temprature: 200,
    layerNo: 1,
  },
];

// 返回一个温度塔的数据
export const getTempratureTowerDate = (towerType?: number): TempratureTowerDataProps[] => {
  return tempratureTowerData;
};
