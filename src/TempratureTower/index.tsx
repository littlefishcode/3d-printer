import React from 'react';
import { Card, Timeline, message } from 'antd';

import TempratureTable from './TempratureTable';
import type { refProps as tableRefProps } from './TempratureTable';

import UploadGcodeFile from './UploadGcodeFile';
import type { refProps as uploadRefProps } from './UploadGcodeFile';

import DownFile from './DownFile';

import styles from './index.less';
import { TempratureTowerDataProps } from './Data';

type TempratureTowerProps = {};

const download = (filename: string, text: string) => {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
};

const createNewGcodeFile = (oldText: string, tempratureData: TempratureTowerDataProps[]) => {
  var newText = oldText;
  for (var item of tempratureData) {
    const findText = `;LAYER:${item.layerNo}\r\n`;
    const replaceText = `;LAYER:${item.layerNo}\r\nM104 S${item.temprature} T0 ; littlefish custom hot end temp\r\n`;
    newText = newText.replace(findText, replaceText);
  }
  download('newTempratureTower.gcode', newText);
};

/**
 * 控件内容
 * @param props
 * @returns
 */
const TempratureTower: React.FC<TempratureTowerProps> = (props: TempratureTowerProps) => {
  const tableRef = React.useRef<tableRefProps>(null);
  const uploadRef = React.useRef<uploadRefProps>(null);

  const downLoadFile = () => {
    const file = uploadRef.current?.getFile();
    if (file === null || file === undefined) {
      message.error('下载错误，请上传gcode文件。');
    } else {
      const reader = new FileReader();
      reader.readAsText(file, 'utf-8');
      reader.onload = () => {
        if (
          reader.result?.toString() !== undefined &&
          tableRef.current?.getTempratureTowerData() !== undefined
        ) {
          createNewGcodeFile(reader.result?.toString(), tableRef.current?.getTempratureTowerData());
        } else {
          message.error('下载错误，gcode文件为空或温度设置有问题。');
        }
      };
    }
  };

  return (
    <Card title="温度塔生成器" type="inner">
      <Timeline.Item>
        <div className={styles.subTitle}>上传要后期处理的GCode文件：</div>
        <UploadGcodeFile ref={uploadRef} />
      </Timeline.Item>
      <Timeline.Item>
        <div className={styles.subTitle}>指定每层的温度：</div>
        <TempratureTable ref={tableRef} />
      </Timeline.Item>
      <Timeline.Item>
        <div className={styles.subTitle}>下载设置好每层温度的GCode文件：</div>
        <DownFile downLoadFile={downLoadFile} />
      </Timeline.Item>
    </Card>
  );
};

export default TempratureTower;
