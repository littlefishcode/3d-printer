import React from 'react';
import { Typography } from 'antd';
const { Paragraph } = Typography;

type EstepDescProps = {
  oldEstep: number;
  measurement: number;
};

const EstepDesc: React.FC<EstepDescProps> = (props: EstepDescProps) => {
  const { oldEstep, measurement } = props;
  return (
    <div>
      <Paragraph>
        <ul>
          <li>首先要知道最初设置时多少，这里举例是 {oldEstep}</li>
          <li>
            然后根据测算，120mm 剩了 {measurement}mm，也就是按照 {oldEstep} 的速率，挤出了{' '}
            {120 - measurement}mm
          </li>
          <li>
            那么如果要挤出 100mm 需要多少速率呢？
            <br />
            <pre>
              {120 - measurement}/{oldEstep} = 100/a 这里要计算出a
            </pre>
          </li>
        </ul>
      </Paragraph>
    </div>
  );
};

export default EstepDesc;
