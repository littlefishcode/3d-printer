import React from 'react';
import { Typography } from 'antd';
const { Paragraph, Text } = Typography;

type EstepCalculProps = {
  oldEstep: number;
  measurement: number;
};

const calcul = (oldEstep: number, measurement: number) => {
  return ((100 * oldEstep) / (120 - measurement)).toFixed(2);
};

const EstepCalcul: React.FC<EstepCalculProps> = (props: EstepCalculProps) => {
  const { oldEstep, measurement } = props;

  return (
    <Paragraph>
      剩余<b> {measurement}mm </b>耗材，这意味着您挤出了<b> {120 - measurement}mm </b>。您的新
      E-steps 应为<b> {calcul(oldEstep, measurement)} </b>在终端中输入以下内容：
      <br />
      <pre>M92 E{calcul(oldEstep, measurement)}</pre>
      随后由 M500 保存到 EEPROM。
      <br />
      <pre>M500</pre>
      您可能希望使用新的 E-steps 值重复此测试以进行验证。
      <br />
      <Text italic disabled>
        特别说明：Prusa 在某些打印机（例如 Mini）上禁用了 M500 保存到
        EEPROM。在这些情况下，必须将上述 M92 gcode 添加到切片器的起始 gcode
        中，以便在每次打印之前读取。
      </Text>
    </Paragraph>
  );
};

export default EstepCalcul;
