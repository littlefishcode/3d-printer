import React from 'react';
import { Typography, Table, Badge } from 'antd';
const { Paragraph, Text } = Typography;
import type { ColumnsType } from 'antd/es/table';

interface DataType {
  key: string;
  layerHeight: string | number; //Layer height
  errorPer10cm: string | number; //Error over 10cm
  stepCount: string | number; // StepCount
  stepLenght: string | number; //Step length
  isGood: Boolean; // 是否最佳
}

type LayerHeightProps = {
  StepSize: number; //步距角
  DesiredHeight: number; //预设层高
  LeadscrewPitch: number; //丝杆导程
  PulleyMotor: number; //齿轮传动比：电机
  PulleyLeadscrew: number; //齿轮传动比：丝杆
};

const EstepCalcul: React.FC<LayerHeightProps> = (props: LayerHeightProps) => {
  const { StepSize, DesiredHeight, LeadscrewPitch, PulleyMotor, PulleyLeadscrew } = props;

  //每步的长度mm = (螺纹转一圈的长度/电机转一圈的步数)*(螺纹齿数/电机齿数)
  var StepLenght =
    ((LeadscrewPitch * 1000000) / (StepSize * 1000000)) * (PulleyLeadscrew / PulleyMotor);
  //每层需要几步 = 目标层高/每步的长度 这个可能是小数，如果是小数就会出现误差，因为不会走半步的。
  var LenghtsPerLayer = (DesiredHeight * 1000000) / (StepLenght * 1000000);
  // 向下求整
  var FloorLenghtsPerLayer = Math.floor(LenghtsPerLayer);
  // 向上求整
  var CeilLenghtsPerLayer = Math.ceil(LenghtsPerLayer);

  var FloorHeight, CeilHeight, errorSign, errorPer10cm;

  var errorPerStep, error10cmsteps, isGood;

  // 如果发现每层的步数是整数，那么就挺好的
  if (FloorLenghtsPerLayer == CeilLenghtsPerLayer) {
    isGood = true;

    //向下减少一步
    FloorLenghtsPerLayer--;
    //向上增加一步
    CeilLenghtsPerLayer++;

    //最小的层高
    FloorHeight = FloorLenghtsPerLayer * StepLenght;
    //最高层高
    CeilHeight = CeilLenghtsPerLayer * StepLenght;
    errorSign = 0;
    errorPer10cm = 0;
  } else {
    // 不是理想的层高
    isGood = false;

    //最小层高，这里取4位
    FloorHeight = FloorLenghtsPerLayer * StepLenght;
    FloorHeight = FloorHeight.toPrecision(4);
    //高层高，这里取4位
    CeilHeight = CeilLenghtsPerLayer * StepLenght;
    CeilHeight = CeilHeight.toPrecision(4);

    //每步偏差= 小数点的数值
    errorPerStep = LenghtsPerLayer - Math.floor(LenghtsPerLayer);

    //计算是增加了，还是减少了
    errorSign = '-';
    if (errorPerStep > 0.5) {
      errorPerStep = 1 - errorPerStep;
      errorSign = '+';
    }
    //10cm的偏差 = 10cm要走几步*每步的偏差值*每步长度
    error10cmsteps = 100 / DesiredHeight;
    errorPer10cm = error10cmsteps * errorPerStep * StepLenght;
    errorPer10cm = errorSign + errorPer10cm;
  }

  const data: DataType[] = [
    {
      key: 'floor',
      layerHeight: FloorHeight,
      errorPer10cm: 0,
      stepCount: FloorLenghtsPerLayer,
      stepLenght: StepLenght,
      isGood: true,
    },
    {
      key: 'original',
      layerHeight: DesiredHeight,
      errorPer10cm: errorPer10cm,
      stepCount: LenghtsPerLayer,
      stepLenght: StepLenght,
      isGood: isGood,
    },

    {
      key: 'ceil',
      layerHeight: CeilHeight,
      errorPer10cm: 0,
      stepCount: CeilLenghtsPerLayer,
      stepLenght: StepLenght,
      isGood: true,
    },
  ];

  const columns: ColumnsType<DataType> = [
    {
      title: '层高',
      dataIndex: 'layerHeight',
      key: 'layerHeight',
      render: (_, record) => {
        if (record.isGood) {
          return <Badge status="success" text={record.layerHeight} style={{ color: '#237804' }} />;
        } else {
          return <Badge status="error" text={record.layerHeight} style={{ color: 'red' }} />;
        }
      },
    },
    {
      title: '10cm后的偏差',
      dataIndex: 'errorPer10cm',
      key: 'errorPer10cm',
      render: (text) => <>{text}mm</>,
    },
    {
      title: '每层步数',
      dataIndex: 'stepCount',
      key: 'stepCount',
    },
    {
      title: '每步长度',
      dataIndex: 'stepLenght',
      key: 'stepLenght',
      render: (text) => <>{text}mm</>,
    },
  ];

  return (
    <div>
      <Table columns={columns} dataSource={data} pagination={false} size="small" />
      <br />
      <Paragraph>
        <b>计算说明</b>
        <br />
        <br />
        <p>
          <b>电机每步的长度mm</b> = (螺纹转一圈的长度/电机转一圈的步数)*(螺纹齿数/电机齿数)
          <pre>
            ({LeadscrewPitch}/{StepSize})*({PulleyLeadscrew}/{PulleyMotor})={StepLenght}mm
          </pre>
        </p>
        <p>
          <b>每层需要几步</b>=目标层高/每步的长度。如果是
          <span style={{ color: 'red' }}>小数就会出现偏差</span>，因为Z 轴仅以完整的步长增量移动。
          <pre>
            {DesiredHeight}/{StepLenght}={LenghtsPerLayer}步
          </pre>
        </p>
        <p>
          <b>向下或向上取整数，以便获得相邻的最佳佳层高</b>
          <pre>
            当前步数：{LenghtsPerLayer} 当前层高：{DesiredHeight} <br />
            向下最佳层高： {FloorLenghtsPerLayer}*{StepLenght}={FloorHeight} <br />
            向上最佳层高： {CeilLenghtsPerLayer}*{StepLenght}={CeilHeight}
          </pre>
        </p>
        <p>
          <b>计算数可能的偏差</b>
          <br />
          <pre>
            计算出到底是往多里偏差还是往小里偏差：{errorPerStep} = {LenghtsPerLayer} - Math.floor(
            {LenghtsPerLayer});
            <br />
            因为固件要四舍五入，如果上面的数大于0.5，那么就是往多里偏差了，否则往小里偏差。如果是0那么没有偏差。
            <br />
            总的偏差= 每移动一层偏差了多少 * 10cm共有多少层 = ({errorPerStep}*{StepLenght})*(100/
            {DesiredHeight})={errorPer10cm}mm
          </pre>
        </p>
      </Paragraph>
    </div>
  );
};

export default EstepCalcul;
