import React from 'react';
import { Card, InputNumber, Form, Tabs, Select, Row, Col } from 'antd';

import Calcul from './Calcul';

type LayerHeightProps = {
  StepSize: number; //步距角
  DesiredHeight: number; //预设层高
  LeadscrewPitch: number; //丝杆导程
  PulleyMotor: number; //齿轮传动比：电机
  PulleyLeadscrew: number; //齿轮传动比：丝杆
};

const StepSizeOptions = [
  {
    value: '200',
    label: '1.8°-每圈转200次 42步进电机',
  },
  {
    value: '400',
    label: '0.9°-每圈转400次',
  },
  {
    value: '48',
    label: '7.5°-每圈转48次',
  },
];

const layout = {
  labelCol: { span: 10 },
  wrapperCol: { span: 14 },
};
const tailLayout = {
  wrapperCol: { offset: 12, span: 12 },
};

const LayerHeight: React.FC<LayerHeightProps> = (props: LayerHeightProps) => {
  const [form] = Form.useForm();
  const [StepSize, setStepSize] = React.useState(props.StepSize);
  const [DesiredHeight, setDesiredHeight] = React.useState(props.DesiredHeight);
  const [LeadscrewPitch, setLeadscrewPitch] = React.useState(props.LeadscrewPitch);
  const [PulleyMotor, setPulleyMotor] = React.useState(props.PulleyMotor);
  const [PulleyLeadscrew, setPulleyLeadscrew] = React.useState(props.PulleyLeadscrew);

  const items = [
    {
      label: '计算结果',
      key: 'item-1',
      children: (
        <Calcul
          StepSize={StepSize}
          DesiredHeight={DesiredHeight}
          LeadscrewPitch={LeadscrewPitch}
          PulleyMotor={PulleyMotor}
          PulleyLeadscrew={PulleyLeadscrew}
        />
      ),
    }, // 务必填写 key
  ];

  return (
    <Card title="最佳层高" type="inner">
      <Form name="basic" {...layout} initialValues={props} autoComplete="off" form={form}>
        <Row>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item label="电机一圈转动次数" name="StepSize">
              <Select options={StepSizeOptions} onChange={(value) => setStepSize(value)} />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item label="预设层高(mm)" name="DesiredHeight">
              <InputNumber
                min={0.01}
                max={10}
                step="0.01"
                onChange={(value) => setDesiredHeight(value ? value : 1)}
              />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item
              label="丝杆导程(mm)"
              name="LeadscrewPitch"
              rules={[{ required: true, message: '请输入丝杆每圈导程!' }]}
            >
              <InputNumber
                min={1}
                max={100}
                step="1"
                onChange={(value) => setLeadscrewPitch(value ? value : 1)}
              />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Form.Item label="电机与丝杆传动比">
              <Form.Item
                name="PulleyMotor"
                rules={[{ required: true, message: '请输入传动比' }]}
                style={{ display: 'inline-block' }}
              >
                <InputNumber
                  min={1}
                  max={100}
                  step="1"
                  onChange={(value) => setPulleyMotor(value ? value : 1)}
                />
              </Form.Item>

              <Form.Item
                name="PulleyLeadscrew"
                rules={[{ required: true, message: '请输入传动比' }]}
                style={{ display: 'inline-block', margin: '0 3px' }}
              >
                <InputNumber
                  min={1}
                  max={100}
                  step="1"
                  onChange={(value) => setPulleyLeadscrew(value ? value : 1)}
                />
              </Form.Item>
            </Form.Item>
          </Col>
        </Row>
      </Form>

      <Tabs items={items} />
    </Card>
  );
};

export default LayerHeight;
