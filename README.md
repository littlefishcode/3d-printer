# 3d-printer

我叫小鱼儿，是一个学生，在学习 3D 打印机过程中收集了一些资料。

碰到一个无量商家，就把网站闭源了。

我把它做成了一个网站，可以[直接访问](https://dolphin.littlefish.love/)。

![](./public/home.jpg)

也可以按照下面的内容，下载源代码，在本地运行。

## Getting Started

Install dependencies,

```bash
$ npm i
```

Start the dev server,

```bash
$ npm start
```

Build documentation,

```bash
$ npm run docs:build
```

Run test,

```bash
$ npm test
```

Build library via `father`,

```bash
$ npm run build
```
