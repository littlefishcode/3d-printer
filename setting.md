## 引入 antd 的方法 2

```
  "devDependencies": {
    "antd": "^4.23.2",
    "babel-plugin-import": "^1.13.5",
```

要使用图标

```
  "dependencies": {
    "@ant-design/icons": "^4.7.0",
    "react": "^18.0.0"
  },
```

修改.umirc.ts

```
  extraBabelPlugins: [
    [
      'import',
      {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: 'css',
      },
    ],
  ],
```

修改一个文件

```
import React from 'react';
import { Button } from 'antd';

import { Foo } from '3d-printer';

export default () => {
  return (
    <div>
      <Foo title="First Demo" />
      <Button>搭建第一个按钮</Button>
    </div>
  );
};


```

## 引入 tailwind 的方法

package.json

```
  "devDependencies": {
    "tailwindcss": "npm:@tailwindcss/postcss7-compat",
```

添加：tailwind.config.js 文件

修改.umirc.ts

```
  // 引入tailwind
  extraPostCSSPlugins: [
    require('tailwindcss')({
      config: './tailwind.config.js',
    }),
  ],
```

使用

```
# tailwind.css
@tailwind base;
@tailwind components;
.btn {
  @apply px-4 py-2 bg-blue-600 text-white rounded;
}
@tailwind utilities;


#index.less
.title {
  background: rgb(121, 242, 157);
}


#index.tsx
import React from 'react';

import styles from './index.less';

import tw from './tailwind.css';

type DescProps = {
  aim: string;
  when: string;
  tools: string;
};

const Desc: React.FC<DescProps> = (props: DescProps) => {
  console.log(props);
  const { aim, when, tools } = props;
  console.log('------------------------------------');
  console.log(aim);
  return (
    <div>
      <div className={styles.title}>目标</div>
      <div>{aim}</div>
      <div className={tw['text-4xl']}>什么时候？</div>
      <div>{when}</div>
      <div>工具</div>
      <div>{tools}</div>
    </div>
  );
};

export default Desc;
```
