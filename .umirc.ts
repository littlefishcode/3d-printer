import { defineConfig } from 'dumi';

export default defineConfig({
  title: 'Dolphin',
  favicon: '/logo2.png',
  logo: '/logo2.png',
  outputPath: 'docs-dist',
  mode: 'site',

  //引入antd
  extraBabelPlugins: [
    [
      'import',
      {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: 'css',
        dark: true,
      },
    ],
  ],

  // more config: https://d.umijs.org/config
  ssr: {},
  exportStatic: {},
  analytics: {
    // 百度统计代码，配置后会启用
    baidu: 'e8c5d0126401f0f55ad4f04df568e140',
  },

  // 引入tailwind
  extraPostCSSPlugins: [
    require('tailwindcss')({
      config: './tailwind.config.js',
    }),
  ],

  navs: [
    // null, // null 值代表保留约定式生成的导航，只做增量配置
    {
      title: 'Calibration',
      path: '/calibration',
    },
    {
      title: 'Troubleshooting',
      path: '/troubleshooting',
    },
    {
      title: 'Upgrade Guides',
      path: '/upgrade',
    },
    {
      title: 'Community',
      path: '/community',
    },
    {
      title: 'Voron2.4',
      path: 'https://seahorse.littlefish.love/',
    },

    {
      title: 'Gitee',
      path: 'https://gitee.com/littlefishcode/3d-printer/',
    },
  ],

  //下面是高级用法-----------------------------------------------
  styles: [
    `
    .__dumi-default-navbar{
      padding: 0 28px !important;
    }

    .__dumi-default-navbar-logo{
      color:#45124e !important;
    } 
    [data-prefers-color=dark] .__dumi-default-navbar-logo {
      color: rgba(255, 255, 255, 0.85) !important;
    }


    .__dumi-default-layout-hero{
      background-image: url(//img.alicdn.com/imgextra/i4/O1CN01ZcvS4e26XMsdsCkf9_!!6000000007671-2-tps-6001-4001.png);
      background-size: cover;
      background-repeat: no-repeat;
      padding: 120px 0 !important;
    }
    .__dumi-default-layout-hero h1{
      color:#45124e !important;
      font-size:80px !important;
      padding-bottom: 30px !important;
    }

    .__dumi-default-layout-hero .markdown{
      color:#454d64 !important;
    }

    .__dumi-default-layout-features > dl a dt {
      text-decoration: none !important;
      color: #454d64  !important;
      transition: opacity 0.2s;
    }

    [data-prefers-color=dark] .__dumi-default-layout-features > dl dt {
      color: rgba(255, 255, 255, 0.85) !important;
    }  

    .__dumi-default-layout-features > dl > a:-webkit-any-link{
      text-decoration: none !important;
    }

    `,
  ],
});
