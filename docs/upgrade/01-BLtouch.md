# 自动调平

原版叫做 BLtouch，国内改版后叫做 3Dtouch.

## 1、概要说明

原版制造商网站： [BLtouch](https://www.antclabs.com/bltouch-v3)

### 1.1 购买说明

除了实际的 BLtouch 之外，还需要一根延长线才能返回到主板。对于大多数打印机，此值应至少为 1.5m。对于较大的打印件，建议使用 2 米。

除了原版的 BLtouch。还存在各种克隆版本（通常称为“3D Touch”），这些版本质量参差不齐。对于某些人来说，他们工作得很完美，而对于其他人来说，他们除了遇到麻烦之外什么都没有。

### 1.2 主要作用

BLtouch 是一种自动床平台调平探头。当连接到打印头时，它可以在床周围以网格模式移动，测量每个探测点的高度，然后创建床的轮廓图。当新打印开始时，此等高线图可以应用于第一层，根据需要上下移动喷嘴以补偿翘曲或不平坦的床。

### 1.3 优势

BLtouch 非常方便，因为它消除了手动调平 3D 打印机床的需要。在床翘曲的情况下，手动调平永远不会提供完美的第一层，而 ABL 探头是实现这一目标的唯一方法。与其他非接触式 ABL 探头相比，BLtouch 在探测时用其引脚物理接触床。因此，它不关心床的表面是什么或它的当前温度。BLtouch 允许更换或更改打印表面，而无需重新校准。

### 1.4 弊端

有一种观点认为，将 ABL 与第一台打印机一起使用的新手会阻止他们学习手动调平床的基本技能，因此不会具有与其他用户相同的理解和解决问题的能力。

### 1.5 入门视频

这里有一个视频，讲解了如何安装 3Dtouch，讲的非常详细。

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=506988010&bvid=BV18g411A7MR&cid=450046178&page=1"
        scrolling="yes"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

## 2、硬件安装

需要将 BLtouch 安装到机器的打印头上。

- 理想情况下，BLtouch（或任何其他 ABL 探头）将尽可能靠近喷嘴安装，但不要太近，以免因暴露在高温下而失效。
- 加热器块上的硅胶袜可以在这方面提供帮助。
- BLtouch 应牢固固定。任何松动或摆动都会破坏准确性和可重复性。
- 安装时的其他考虑因素是确保 BLtouch 不会太靠近热端，以免受到高温的损坏，并且其位置不会干扰归位。

由于 BLtouch 非常受欢迎，因此在 Thingiverse 和其他 3D 文件共享站点上可以使用各种打印安装座。以下是一些示例：

- [Ender 3 (Pro) / CR-10](https://www.thingiverse.com/thing:3003725/)
- [Ender 3 V2](https://www.thingiverse.com/thing:4462870)
- [Ender 5 (Pro)](https://www.thingiverse.com/thing:3388571/)
- [Artillery3D X1](https://www.thingiverse.com/thing:3716043)

BLtouch 尖端相对于喷嘴尖端的垂直安装高度对于准确性非常重要。Antclabs 在他们的网站上有一个图表，建议目标高度为 2.3 至 4.3mm。

![](./imgs/bltouchdimensions.jpg)

关闭电源后，向下放下打印头，直到喷嘴放在床上。如果您的 BLtouch 安装座是可调节的，您可以将一个大小合适（3 毫米是理想尺寸）的内六角扳手平放在床上，然后将 BLtouch 向下滑动，直到尖端接触。

对于**不可调节的安装座**，可以使用垫圈将位置向下填充到规格中。

如果吸头**位置太低**，则需要一个新的安装座，以防止吸头撞到床/打印部件并损坏。

![](./imgs/bltouchspacing.jpg)

### 2.1 裹线管

由于这个线到主板的距离很长，为了避免被缠绕，建议包裹起来。

![](./imgs/loomwraps.jpg)

## 3、主板接线

BLtouch 需要 5 根电线。三个用于控制它（红色，黄色和棕色），其余两个用于读取它的触发信号（黑色和白色）。

以下是引脚的细分：

- <span style="color:red; font-weight:900;">红色：</span> 5V 电源至蓝牙。（某些主板提供 3.3V 电压，BLtouch 可配置为在此逻辑电平下工作）。
- <span style="color:brown; font-weight:900;">棕色：</span> 接地到布勒图。与红线共轭提供电源。
- <span style="color:yellow; font-weight:900; background-color:black;">黄色：</span> 控制从固件到 BL 触控的信号。使用脉宽调制 （PWM） 来控制 BLtouch，就像使用 PWM 控制伺服一样。
- <span style="color:white; font-weight:900; background-color:black;">白色：</span> 从 BL 触控到主板的触发信号。
- <span style="color:black; font-weight:900;">黑色：</span>从 BL 触控到主板的触发信号接地。

大多数现代 32 位主板都有用于 BLtouch 的专用端口，通常标记为“BLtouch”、“探头”或“伺服”。这可以是一个三针端口，仅用于红色、棕色和黄色电线，也可以是一个五针端口，用于所有五根电线。

如果您的主板没有专用的 BLtouch 端口，您很可能仍然可以使用它，但您可能需要查阅主板或处理器的手册或数据表。5V 和 GND 可以从任何已识别的备用引脚中获取。您所需要的只是一个备用的 PWM 引脚。PWM 代表[脉宽调制](https://en.wikipedia.org/wiki/Pulse-width_modulation)，涉及在各种占空比下循环打开和关闭逻辑信号。此方法用于设置舵机上的角度，也用于通过黄色导线控制 BLtouch。

有了一些基本知识，您就可以检查主板处理器的数据表，看看哪些备用针脚支持 PWM。

### 3.1 多余的黑白接线

如果是 5 根线，将黑白 BLtouch 电线插入 Z 端部插头还是专用插头？

诚实的答案是，这些选项中的任何一个都有效。固件只需要一个输入引脚来接收来自 BLtouch 的触发信号，它不在乎它是哪个引脚。

就个人而言，我更喜欢使用正常的 Z 轴端顶端口。这是因为它简化了固件中所需的更改（下面介绍了两个版本）。它还可以防止任何使旧的 Z 端挡开关保持连接和潜在干扰的机会。

## 4、修改固件

### 4.1 修改代码入门

首先，如果您是固件新手，我有一些初学者指南，但是[更新的 Marlin 固件设置指南 - VS 代码和自动构建 Marlin](https://www.bilibili.com/video/BV1z34y117aq)可能是最好的起点，因为它涵盖了安装软件以及如何使用示例配置来获得打印机的良好起点。

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=808452265&bvid=BV1z34y117aq&cid=493821800&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

获得 Marlin 基线版本后，对于 BLtouch，必须进行以下更改：

### 4.2 必须修改的内容

启用 bltouch

![](./imgs/bltouch-firmware-1.jpg)

启用自动调平

![](./imgs/bltouch-firmware-2.jpg)

定义 Z 轴归位标记

![](./imgs/bltouch-firmware-3.jpg)

### 4.3 黑白线配置

分下面两种情况，看适合那种情况，就做出相应的修改。

#### 接到 Z 限位开关

将黑白线接到 Z 限位开关

默认情况下，以下内容应已准备就绪：

![](./imgs/bltouch-firmware-8.jpg)

#### 接到专用端口

您需要查找端口使用的引脚的名称，然后进行以下更改：

![](./imgs/bltouch-firmware-9.jpg)

### 4.4 自定义引脚接黄线(很少见)

如果您使用自定义 PWM 引脚来连接 BL 点黄色电线。

您需要查找 PIN 的名称，然后在主板的 PIN 文件中输入该名称。确保这是对此引脚的唯一引用。取消注释其他引用以避免冲突。

![](./imgs/bltouch-firmware-10.jpg)

### 4.5 可选设置

#### 启动 LCD 菜单

如果您使用的是传统 LCD，则可以启用 ABL 特定的子菜单：

![](./imgs/bltouch-firmware-4.jpg)

#### Z 轴偏移

要更轻松地校准探头的 Z 轴偏移（在前面的视频中介绍），请启用以下功能：

![](./imgs/bltouch-firmware-5a.jpg)

![](./imgs/bltouch-firmware-5.jpg)

![](./imgs/bltouch-firmware-6.jpg)

如果已经测量了 X 和 Y 探头偏移，则可以立即输入。但是，这不是强制性的，因为它可以在固件更新后完成。稍后将详细介绍探头偏移。

![](./imgs/bltouch-firmware-7.jpg)

## 5、高级定制

默认情况下，探测网格为 3 x 3。对于小型打印机，这很好，但您可能希望以更长的探测序列为代价来提高分辨率。这可以在固件中更改：

![](./imgs/bltouch-firmware-11.jpg)

默认情况下，探测序列相当慢。同样，我们可以在固件中对其进行自定义。您在这里的选择将是速度和准确性之间的权衡：

![](./imgs/bltouch-firmware-12.jpg)

默认情况下，探测网格的边界将偏移到探头相对于喷嘴的位置。我个人更喜欢对称的探测网格。在固件中，您可以为网格设置手动边界。只需记住探头 X/Y 偏移量即可。当探头距离喷嘴侧面 40mm 时，您不希望请求从边缘探测 20mm。最好是，您会收到错误。最坏的情况是，探头漏掉床，喷嘴与床发生碰撞。

![](./imgs/bltouch-firmware-13.jpg)

还有一个名为“淡入淡出高度”的参数，可以从 LCD 或使用[**M420 Z**](https://marlinfw.org/docs/gcode/M420.html) gcode 命令进行设置。第一层实际上不会完全平坦，因为应用了补偿来匹配测量的床轮廓。然而，我们不一定希望每一层都与印刷品的其余部分的轮廓相匹配。淡入淡出高度是打印机在沿床轮廓到在完全平坦的平面上打印之间转换的垂直距离。请记住在更改淡入淡出高度后保存到 EEPROM。

## 6、校准

连接所有内容并刷新更新的固件后，仍会对探头的偏移进行一些校准。可以参考前面的入门视频。

需要执行操作有这些：

- 测量并输入我们的 X/Y 探头偏移。
  - 这可以通过终端上的[**M851**](https://marlinfw.org/docs/gcode/M851.html) gcode 命令或通过 LCD 屏幕完成。即使您之前在编译固件时输入了此值，也请仔细检查值是否正确。
  - 更改偏移量时，不要忘记保存到 EEPROM。
- 调整探头的 Z 偏移量，以在第一层上实现完美的挤压量。
  - 您可以使用 Marlin Z 偏移向导执行此操作和/或在第一个图层下降时执行实时调整。
  - 在任何一种情况下，都不要忘记将此值保存到 EEPROM，以便它保持持久性。

## 7、第一次归零

由于原先的限位开关已经被替换，为了避免喷头被撞坏，需要在打印机旁边观察第一次归零。

<Alert type="error">

第一次将打印机归零时，最安全的方法是将喷嘴放在床上方很远的地方。然后用手在半空中触发探头。如果无法阻止 z 轴移动，要赶紧拔掉电源。

</Alert>

如果没有问题，启动[**G29**](https://marlinfw.org/docs/gcode/G029.html)命令将启动对床的探测。（可选）保存到 EEPROM 将存储探测到的网格。

在探测之前，最好将床加热到打印温度。这是由于床表面在热时膨胀和/或翘曲。我们希望尽可能匹配打印条件

## 8、切片配置

关于如何使用 BLtouch，我们有两种选择：

### 8.1 每次打印前进行探测

此选项的优点是可提供最准确的结果，并且适合具有可能不稳定的床（可能会在打印之间移动）的打印机。缺点是增加了开始打印的时间。

为此，必须将 **G29** （ABL） 放置在切片器的起始 **gcode 部分中 G28**（归位）之后的行上。**G28** 取消所有探测网格，因此它必须按此顺序排列。

![](./imgs/g29-start-gcode.jpg)

### 8.2 只探测一次

探测一次，并在打印开始时恢复保存的网格.

如果您的床是稳定的，您可以使用**G28**回家，然后探入床以创建带有**G29**的网格，然后使用**M500**将网格保存到 EEPROM。所有这些都可以通过 LCD 控件而不是 g 代码来完成。在切片器开始 gcode 中，您可以在 **G28** 之后的行中添加 [**M420 S1**](https://marlinfw.org/docs/gcode/M420.html)，这将加载以前保存的网格以用于在当前打印中进行补偿。

如有必要，您可以不时重新探查床并存储网格以保持精度。

![](./imgs/m420-S1.jpg)

## 9、故障排除

添加 BLtouch 做了好多步骤，这意味着也有很多可能出错的地方。如果出现故障，可以参考校准栏目。
