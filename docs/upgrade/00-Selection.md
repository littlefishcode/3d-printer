# 选型

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

打印机的选择有两个方向：

- 选择 diy 套件，自己组装。
- 选择成品机器，专注打印。

## 1、需求方向

如果你是一个机械迷，不是为了打印产品，而是为了组装机器，那你可以选择 dir 套件，充分享受组装的乐趣。

如果你为日常打印一些内容，那么就买成品机，更加专注 3D 设计软件与创意，不用把大把的时间花费在调试机器上。

## 2、机器选购

假设你主要是以打印作品为主。首先在你周边有一个熟悉 3d 打印机的人，让他帮你选择，不然会被这个混乱的市场给挑花眼的。

如果有人知道，可以先从咸鱼上买一个二手的，价格在 300 左右的入门级别 dir 打印机，自己尝试去拆解组装，这个过程了解一下打印机的主要组成部分，然后熟悉一下打印机。这个过程不要超过 3 个月。

等充分了解了自己是否要进行下一步，可以去买一个 2500 元主流的成品机，然后就愉快的打印作品把，把精力放在设计软件、切片软件上。

## 3、评价标准

主要是家用为主，不打印有毒材料，例如 ABS。

### 3.1 主观评测

- 打印速度要快：60 以上最好。
- 打印效果要好
- 打印尺寸要超过 20cmX20cm
- 噪音要小
- 占地面积小，放在家里。

有很多低价的 dir 组装机不适合长时间用，现在出去吃顿饭可能就要花几百元，如果为了一个几百元的机器花上一天或者两天的时间，那就很糟糕了。低价的机器为了赚取噱头，都采用的价格低廉的元器件，出现问题调试起来非常麻烦。

### 3.2 核心技术指标

| 编号 | 项目 | 重要度 | 备注 |
| --- | --- | --- | --- |
| 1 | 有效打印面积 | 5 | 5 分：建议 200mmX200mm 以上。 |
| 2 | 打印结构 | 5 | 5 分：corexy/UM/Materbot/voron 平台升降<br/>4 分：voron2.4/delta <br/>3 分：I3 结构<br/>2 分：t 机构，i3 单悬臂 |
| 3 | 调平难度 | 5 | 5 分：自动调平<br/>4 分：4 点手动调平 |
| 4 | 平台平整度 | 5 |  |
| 5 | 挤出方式 | 5 | 远程与近程 |
| 6 | 打印速度 |  | 主流的打印机是 60<br/>90 已经很优秀了<br/>120 噪音很小，已经很优秀了。 |
| 7 | 喷头散热 |  | 散热无死角 bengo<br/>三项散热 prusa i3 <br/>对称散热<br/>单向散热 ender3 v2<br/>顺风道散热 |
| 8 | 悬垂冷却效果 |  | 50 元 PLA 0.15 层高 60 速度 85 度最好 |
| 9 | z 轴一致性 |  | z 轴层纹有关<br/>丝杆电机 5 分 <br/>丝杆+弹性联轴器<br/>丝杆+刚性联轴器 |
| 10 | 挤出精度 |  | 金属齿轮 BMG 双齿轮挤出 5 分。<br/>塑料 BMG 4 分<br/>全金属轴承单齿轮 |
| 11 | XYZ 轴精度 |  | 线轨 5 分<br/>光轴 4 分<br/>滑轮 3 分 |
| 12 | 组装难度 |  | 5 分钟组装好 5 分<br/>10 分钟 4 分<br/>30 分钟 3 分<br/>1 小时以上 0 分 |
| 13 | 控制方式 |  | 手机远程控制 5 分<br/>wifi 4 分<br/>触控屏 3 分 |
| 14 | 控件利用率 |  | 70% 5 分 |
| 15 | 功能改装潜力 |  | 如果是完美的，追加 5 分。只要有一项改装就扣分。 |
| 16 | 整体工作噪音 |  | 40 分贝 5 分：prusa i3 stealth mode |
| 17 | 噪音源 |  | 风扇的噪音。 |
| 18 | 自动断电 |  |  |
| 19 | 价格 |  |  |
| 20 | 外观 |  |  |

### 3.3 扩展加分项

| 编号 | 项目         | 重要性 | 备注 |
| ---- | ------------ | ------ | ---- |
| 1    | 封箱         |        |      |
| 2    | 空气过滤     |        |      |
| 3    | 高速打印喷头 |        |      |
| 4    | 打印照明     |        |      |
| 5    | 摄像头监控   |        |      |
| 6    | 换头打印     |        |      |
| 7    | 换色打印     |        |      |
| 8    | z 轴同步带   |        |      |
| 9    | 其他重新项目 |        |      |

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=470481269&bvid=BV1kT411378S&cid=758114097&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

## 4、主流机型

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=629083700&bvid=BV1ft4y1B71t&cid=293985359&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

### 4.1 T 型或 I 型机

T 型机入门机型不能用。 I 型机器如果是好的成品机，还可以用用。

### 4.2 Corexy 结构

速度高，打印精度也高。皮带选择很重要。 BLA 的开源项目。

[800 块做一台又大又快又稳的 CoreXY 结构 3D 打印机](https://www.bilibili.com/video/BV15N411o7ou/?spm_id_from=333.999.0.0&vd_source=bafb6a620321f0a20f64f85a20c29a4b)

还有稍微复杂的 um2 结构

[DIY 一台打印的时候也可以移动的 UM 结构 3D 打印机！](https://www.bilibili.com/video/BV11o4y117jg/?p=2&spm_id_from=pageDriver&vd_source=bafb6a620321f0a20f64f85a20c29a4b)

### 4.3 voron 结构

终极选项，等过几年再说。

VoronDesign 官网：www.vorondesign.com

- [卧龙 Tiny-M 人人都可 DIY 组装的 3D 打印机 Voron 0.1 放大版](https://www.bilibili.com/video/BV1YY4y1z7mi/?spm_id_from=333.788&vd_source=bafb6a620321f0a20f64f85a20c29a4b)
- [从零开始组装一台 3D 打印机，还可以多色打印](https://www.bilibili.com/video/BV1GF411t7pF)
- [浅谈一下 Voron2.4 打印机，让入门者有个清晰的定位和了解](https://www.bilibili.com/video/BV1KL4y1N76C)

VoronV0.1 速度为 240.mm/s 的开源打印机，桌面级 3D 打印机。VORON2.4 目前官方提供了三个尺寸 250、300 和 350。采用全封闭设计，采用直线导轨运动，XY 属于 corexy 结构，Z 轴四根皮带同步传动，磁性打印平台，自动调平，支持 Klipper 固件。 2.4 版最大优点就是宜用简洁，减少了华而不实的设计，增强组装接合强度，大量减少了打印件，后期调试也变的更加方便，可以减少很多拆装步骤，甚至不用拆装就可以调整皮带松紧。
