# 挤出机喷嘴优化

## 1、MK8 套装

入门式打印机的常见配置：组装简单、价格便宜、效果一般，所以要进行好多优化。

<img src="./imgs/mk8-1.jpg" style="zoom:33%;" />

![](./imgs/mk8-2.jpg)

### 1.1 问题与改进措施

其实对入门 dir 打印机，建议放弃改造，玩玩就行了。

- 挤出机弹簧过紧，造成进料退料困难。
  - 自己打印一个短的螺丝就行。
- 风扇声音大
  - 自己买个好的风扇就可以了。
- 可控风扇的风对打印件散热不好，造成悬空打印不理想。
  - 打印环绕风扇喷嘴

## 2、VoronM4

Voron 设计的挤出机：https://vorondesign.com/voron_m4

可以自己做一个。

- https://github.com/VoronDesign/Mobius-Extruder/tree/m4/CAD 3D 设计文件
