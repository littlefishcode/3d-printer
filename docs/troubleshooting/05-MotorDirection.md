# 电机方向不对

## 1、测试

可以使用 3D 打印机的 LCD 菜单或触摸屏，或控制软件手动控件每个轴的移动，以查看轴是否按预期移动。请记住：

- 如果移动方向反转，请立即关闭电源，以避免发生破坏性碰撞。
- 大多数不允许移动小于 0 的值，有些还有软限制，避免移动出框架的范围。
- 可以通过发送命令来移动（例如。向右移动轴 20mm）

```
G91
G1 X20 F360
G90
```

如果方向不对，就要进行修复

## 2、修复

### 2.1 现有菜单

有些打印机自带的有这个功能菜单，在 lcd 屏幕上点击使用就可以了。

### 2.2 修改 Marlin 固件

对于 Marlin 固件，在 **configuration.h** 中，搜索术语**INVERT_X**，以转到如下所示的部分：

![](./imgs/marlinstepperdirection.jpg)

只需将值从 true 切换为 false，反之亦然，即可获得任何反转轴的值。重新编译并刷新更改。

### 2.3 修改主板跳线

如果您没有固件的源代码，则可以选择更改接线。最简单的解决方案是反向插入步进电机，与其使用刀片来修改连接器并对其进行破坏，更好的解决方案是将塑料连接器外壳从主板 PCB 上提起，然后朝另一个方向更换。如下图所示，其中 E 步进端口已反转：

![](./imgs/stepperplugreversed.jpg)

### 2.4 接线错误的卡顿

有时，您可能会将步进电机升级到具有不同布线的另一个品牌。

如果没有进行相应的修改，步进电机将来回卡顿，而不是在任一方向上平稳转动。在这些情况下，最常见的解决方法是从连接器上卸下中间的两个引脚并交换它们。这可能很麻烦，但可以用尖嘴钳或者螺丝刀来使用。

![](./imgs/flippedstepperwires.jpg)

如果您的步进电机电流设置得太低，则可能会发生相同的卡顿。在切换导线或增加电流时要小心。

## 3、相关视频

步进电机常见故障分析与处理，这些原因你都知道怎么处理吗？

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=255582046&bvid=BV1sY411E7to&cid=570722167&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

diy3d 打印机步进电机驱动干货分享，你学会了吗？

- 4988 3 到 4 元
- tmc
  - tmc 2208 12 元
  - tmc 2209 多了一个归零功能

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=677151582&bvid=BV11U4y1N7Nt&cid=456931145&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

3d 打印机小知识之如何区分步进电机好坏

大部分使用的是 42 步进电机，可以看看雷赛的 57CM06(机座 57mm 机身长度 41)

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=498958781&bvid=BV1rK411J7Ym&cid=216149706&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

3D 打印机步进电机选型常见类型

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=595696927&bvid=BV1JB4y1U77d&cid=576932290&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```
