# 限位开关/断线检测/探针

把限位开关/断线检测/探针放在一起，是因为他们都可以通过[**M119**](https://marlinfw.org/docs/gcode/M119.html) 进行测试。

```
> M119
Reporting endstop status
x_min: open
y_min: open
z_min: TRIGGERED
z_probe: open
filament: open
```

> 注意：本节仅适用于可手动触发的终端开关。机械微动开关、光开关或磁性开关都属于这一类。它不适用于无传感器归位或用于 Z 归位的 BLtouch。

## 1、检查轴移动方向

除非您确保每个轴都在校正方向上移动，否则无法诊断归位。在尝试以下过程之前，请按照电机方向那章进行测试。

## 2、查看当前状态

确保每个轴都不在原点，这意味着每个触动开关都没有被按下。发送 **M119**，您希望响应为：

```
SENDING:M119
Reporting endstop status
x_min: open
y_min: open
z_min: open
```

手动移动 X 轴到原点，或者用手指按住 X 轴限位开关，然后再次发送**M119**。我们预计输出为：

```
SENDING:M119
Reporting endstop status
x_min: TRIGGERED
y_min: open
z_min: open
```

对每个轴重复此操作应在按下限位开关时显示该轴的**“已触发”**。这是正常行为，您的机器应该正确回归原点，表示限位开关设置正确。

## 3、轴错乱

假设我们按下 X 轴的末端，但 Y 报告为已触发。这说明主板上的线插错了，换一下就好了。

## 4、状态相反

没有归零，却报告 TRIGGERED。归零了，报告 open。则需要在固件中反转结束逻辑。在 **configuration.h** 中，搜索**要**带到以下部分 X_MIN，您可以在其中为反向工作的任何轴在 true/false 之间切换。

![](./imgs/endstopinverting.jpg)

如果固件源代码不能修改，那么只能修改主板上的接线顺序了。公共引脚将始终连接，将 NO（常开）还是 NC（常闭）这两个引脚线交换位置。

其实，也可以新买一个正确的限位开关。

## 5、结束状态从不更改

结束状态从不更改 - 始终报告打开或已触发。原因有很多，大概如下：

- 没有插入到主板上。
- 仔细检查连接后，我们发现按下 **M119** 仍然没有响应，, 我们在固件中可以选择在下拉和上拉电阻之间切换，以防止浮动状态。 在 **configuration.h**中，, 搜索要带有 **ENDSTOPPULLUPS** 的内容, 您可以在其中注释掉 **ENDSTOPPULLUPS**，并取消注释 **ENDSTOPPULLDOWNS**. 在这里，我们有三个选择： using endstoppullups (默认), endstoppulldowns (可能), or neither (不太可能).

![](./imgs/endstoppullups.jpg)

## 6、关于断线传感器

大多数断线传感器都是微动开关，与 XYZ 用到的微动开关都是一样的，只不过采用了花里胡哨的外壳。每个还买的很贵。

这个传感器也可以通过**M119**进行诊断。

```
SENDING:M119
Reporting endstop status
x_min: open
y_min: open
z_min: open
filament: TRIGGERED
```

通过送入或者中断耗材来实现测试。

## 7、Z 探针

在固件中启用 ABL 后，**M119** 还将报告探测状态：

```
SENDING:M119
Reporting endstop status
x_min: open
y_min: open
z_min: open
z_probe: open
```

我们可以使用上述方法对探测器触发进行故障排除。这适用于 TH3D 的 EZabl 或者 Prusa PINDA probe.

[3D 打印机配件 Prusa i3 MK3S/MK3Ss+ super PINDA V2 Sensor 自动调平传感器](https://detail.tmall.com/item.htm?abbucket=4&id=661684913967) 这个东西挺贵 70 多元。

<Alert type="error">
BLtouch 的测试与其他探头略有不同。在专门的章节进行说明。
</Alert>
