# 辅助工具

- 数字卡钳
- 万用表
- 控制台软件
  - [Pronterface](https://www.pronterface.com/) - 免费，适用于 window、Mac 和 Linux。
  - [octoprint 八爪鱼-](https://octoprint.org/) 通常在树莓派上运行的免费主机软件。
  - [Arduino IDE](https://www.arduino.cc/en/software) - 免费，适用于 Windows、Mac 和 Linux。不太直观，但*“工具>串行监视器*”选项将提供一个终端。
- G 代码参考
  - [Marlin](https://marlinfw.org/meta/gcode/)
  - [RepRapFirmware / Duet3D](https://duet3d.dozuki.com/Wiki/Gcode)
  - [Smoothieware](http://smoothieware.org/supported-g-codes)
  - [Klipper](https://github.com/KevinOConnor/klipper/blob/master/docs/G-Codes.md)
  - [Lerdge](https://www.lerdge.com/document/Gcode)
  - [Grbl](https://github.com/gnea/grbl/wiki/Grbl-v1.1-Commands)
- Github 上的资源
  - [BigTreeTech](https://github.com/bigtreetech)
  - [Makerbase MKS](https://github.com/makerbase-mks)
  - [Creality](https://github.com/Creality3DPrinting)
