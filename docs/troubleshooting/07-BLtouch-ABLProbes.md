# 自动调平探头

本页的大部分内容将涉及 BLtouch 的特定测试，但后面有关探测精度的部分适用于任何 ABL 探头。

BLtouch 为 3D 打印机增加了出色的功能。它可以补偿翘曲的床，再多的手动调平也无法修复。

如果您的 BLtouch 未按预期工作，可以看下面的内容：

## 1、BLtouch 的特殊性

其他探头取代了 z 轴的微动开关，其功能相同：它们要么触发，要么不触发，这取决于它们是否感应到探头正下方的床。其他探头仅通过单向通信向固件报告，无论它们是否被触发，而 BLtouch 需要双向通信。固件必须能够向其发送指令，并从中接收触发信号。这就是为什么它具有额外的布线和特殊的 gcode，可以发送来控制它。

## 2、正确接线

BL 触控需要 5 根电线。三个用于控制它（红色，黄色和棕色），其余两个用于读取它的触发信号（黑色和白色）。

![](./imgs/crealityv4bltouchwiring.jpg)

以下是引脚的细分：

- **红色：** 5V 电源至蓝牙。（某些主板提供 3.3V 电压，BLtouch 可配置为在此逻辑电平下工作）。
- **棕色：** 接地到布勒图。与红线共轭提供电源。
- **黄色：** 控制从固件到 BL 触控的信号。使用脉宽调制 （PWM） 来控制 BLtouch，就像使用 PWM 控制伺服一样。
- **白：** 从 BL 触控到主板的触发信号。
- **黑：** 从 BL 触控到主板的触发信号接地。

<Alert>
要使 BLtouch 正常工作，必须正确连接所有这些电线。当打印机通电时，BLtouch将咔哒一声卡入。只有红色和棕色的电线是这种行为所必需的。不要仅仅因为打印机打开时的此顺序就认为您的接线是正确的。
</Alert>

## 3、手工控制 BLtouch

可以手工通过 gcode 命令控制 BLtouch ，来进行错误的排查。

如果 BLtouch 在启动时进行了自检（探头针脚向下，然后向上两次），则表示红色和棕色电线已正确连接，并且 BLtouch 正在通电。

使用终端发送 `M280 P0 S120` 进行扩展自检。这将伸出和缩回探针 10 次。可以通过发送`M280 P0 S160`来过早停止它。

- 如果固件返回一条错误消息，指出它不知道该命令，则您可能尚未正确配置 BLtouch 的固件。
- 如果这些命令发送，但 BLtouch 没有响应，则表示黄线未正确连接。这可能是物理布线问题或固件中指定的错误引脚。请记住，如果为 BLtouch 指定自定义控制引脚，则该引脚必须能够支持 PWM。

请注意，模糊的蓝色 LED 在 BL 触控内部常亮。只有当探头回收时，亮红色的 LED 才会亮起。因此，当伸出探头时，红色 LED 将关闭。红色 LED 亮到您可能不会注意到蓝色 LED 的程度。

一旦我们手动控制了探针，我们就可以测试黑白线，以查看 BLtouch 在触发时是否报告给固件。

当打印机打开且处于空闲状态（BLtouch 引脚缩回）时，发送 **M119** 应返回：

```
z_probe: TRIGGERED
```

如果我们现在通过发送 `M280 P0 S10` 然后再次发出 `M119` 来手动伸出探针，则状态应为：

```
z_probe: open
```

如果我们现在手动将探针与`M280 P0 S160`一起存放，并再次发送`M119`，它应该返回：

```
z_probe: TRIGGERED
```

如果触发状态没有改变，首先要尝试的是将黑白线反转到主板上。另请注意，可以将黑白电线连接到 Z min 端顶端口或您选择的另一个端口，但这必须在固件中进行配置。您会注意到在上面的接线图中，显示了黑白电线的端口，并引用了固件中的匹配行以适合。

> 根据 Marlin M119 文档，如果 BLtouch 向终端报告**已触发，**则处于错误状态。然而，我的 BLtouch 安装工作如上所述，并且多年来一直完美运行，没有错误，所以也许这因主板而异。如果上面的序列对您不起作用，则引脚调试会显示其他 BLtouch 测试方法。

## 4、具体装配

BLtouch 套件带有小弹簧，有时候会出问题。第一步检查打印头和探头安装组件中是否有松动的紧固件。

理想情况下，BLtouch（或任何其他 ABL 探头）将尽可能靠近喷嘴安装，但不要太近，以免因暴露在高温下而失效。加热器块上的硅胶袜可以在这方面提供帮助。人们经常忘记的是，缩回探头的尖端需要与喷嘴的尖端保持一定的距离，如 Antclabs 图所示：

![](./imgs/bltouchdimensions.jpg)

如果您的 BLtouch 距离 2.3 至 4.3 毫米的目标太远，则探测结果可能不准确。有关测量和输入探头 X、Y 和 Z 偏移的指南，请参阅此视频指南：

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=888410410&bvid=BV1XK4y1g7BJ&cid=347906655&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

## 5、探测不准确

首先，要排除没有下面的额问题：

- BLtouch 正确接线和配置 - 通过手动控制和触发测试
- 探头安装牢固 - 没有松动的紧固件
- 探头相对于喷嘴安装正确的高度
- X、Y 和 Z 偏移正确测量并存储在 EEPROM 中（而不仅仅是编译成固件）

除此之外：

- 在开始 gcode 中，在 **G28**（归位）之后，您应该有 **G29**（ABL 的探针床）。默认情况下，G28 会清除所有已保存的网格。
- 手动调平床，使其尽可能接近平坦。ABL 对它可以补偿多少有限制，因此提供合理的起点将有助于该过程。
- 当床达到打印温度时进行探测。大多数 3D 打印机床在加热时容易变形。不要在冷却时探测和存储网格，而是在热打印时应用该网格。
- 探测时临时切断加热床的电源。除 BLtouch 以外的一些探头会受到通过加热床元件的电流的影响。在这种情况下，应将床调到温度，关闭以进行探测，然后在打印前重新打开。
- 在探测期间减慢垂直进给率。为 ABL 设置固件时，可以设置探头行进的垂直速度。向床的下降速度越慢，精度越高，但以牺牲整体网格生成持续时间为代价。
- 有些人对克隆 BLtouch 探针有很好的体验，有些人讨厌它们。我相信他们都是对的。我的意思是克隆探针的质量可能是可变的。

如果选中了所有这些，并且您仍然需要有关探测结果的更多信息，通过在终端中输入**G28**，然后输入**G29**，我们可以接收有关所发生探测的详细信息。输出将根据打印机是否设置为 3x3 网格、4x4 网格等而有所不同。下面是一个示例：

```
Bilinear Leveling Grid:
    0      1      2
0 +0.352 +0.185 +0.265
1 +0.465 -0.007 -0.153
2 +0.915 +0.267 -0.015
```

首先，我需要手动调整此打印机的床，使其更接近平坦。不过，这些数字确实讲述了另一个故事。对于第零行和第 1 列，我们有一个经典的高，低，高翘曲床。床的其余部分似乎更像是一般的倾斜。请记住，点 0，0 可能位于床的左前方，这可能是违反直觉的。

为了获得更直观的表示，用于 Octoprint 的[床展示器](https://plugins.octoprint.org/plugins/bedlevelvisualizer/)插件将提供有吸引力且信息丰富的图表（如下图所示）

![](./imgs/bedvisualizerplugin.jpg)

## 6、M48 重复性测试

我们的另一个选择是启用内置于 Marlin 固件中的专用探测精度测试。在 **configuration.h** 中，搜索 **M48** 以查找以下部分并取消注释，如下所示：

![](./imgs/marlinm48.jpg)

现在，当您通过终端（或通过 LCD 屏幕）发送[**M48**](https://marlinfw.org/docs/gcode/M048.html)时，探头将重复测试同一点并返回结果：

```
Finished!
Mean: -0.006250 Min: -0.010 Max: 0.000 Range: 0.010
Standard Deviation: 0.002795
```

上面的结果来自 Ender 3 上的 BLtouch V3.1。它非常准确，正如您可能期望的那样，ABL 性能是可靠且可重复的。由于探头在同一位置上下移动，我们希望它每次都在同一点触发。可以使用**M48**输入更多参数以自定义精度测试，单击上面的链接以了解更多信息。

## 7、其他参考知识

### 7.1 霍尔传感器

霍尔效应是什么？车速表是如何测量汽车速度的？

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=38599666&bvid=BV1ct411Y7Qc&cid=67854322&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

开关霍尔传感器的检测

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=894846469&bvid=BV1GP4y1u71m&cid=549569630&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

霍尔传感器使用注意事项

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=415050277&bvid=BV18V411y7gh&cid=247949867&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```
