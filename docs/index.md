---
title: Dolphin - 3D打印机的校准与故障排除
order: 10
hero:
  title: Dolphin
  desc: 入门级Marlin3D打印机<br/>的校准与故障排除
  actions:
    - text: 快来吧！
      link: /calibration
features:
  - link: /calibration
    icon: https://img.alicdn.com/imgextra/i2/O1CN016i72sH1c5wh1kyy9U_!!6000000003550-55-tps-800-800.svg
    title: 校准
    desc: 简单易用、检验全面
  - icon: https://img.alicdn.com/imgextra/i3/O1CN01xlETZk1G0WSQT6Xii_!!6000000000560-55-tps-800-800.svg
    title: 故障排除
    desc: 定位准确、案例丰富
    link: /troubleshooting
  - icon: https://img.alicdn.com/imgextra/i1/O1CN01bHdrZJ1rEOESvXEi5_!!6000000005599-55-tps-800-800.svg
    title: 升级
    desc: 不断探索、永无止境
    link: /upgrade
footer: Open-source GPL-3.0 Licensed | Copyright © 2022<br />Powered by [Dolphin](https://dolphin.littlefish.love)
---
