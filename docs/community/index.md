# 声明

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

## 1、关于网站内容

作为一个 3d 打印机的初学者，刚开始最困惑的就是找不到系统的校准说明，于是就把自己学习过程中收集到的资料汇总在一起，只想给自己看。

为了避免商业推广的嫌疑，尽量回避了 3d 打印商或者耗材商的名称。网站制作过程如下：

![](./imgs/make-website.jpg)

## 2、网站的错误与疑问

如果网站有错误或者疑问，可以在 gitee 上提交 bug。 当然也欢迎进行 PR

## 3、如何贡献内容

gitee 有要求，暂时没有设置成 public

作为开源项目，欢迎大家补充网站的内容，如果要补充内容需要具备以下知识：

- 基本技能

  - 会使用 git 与[gitee](https://blog.gitee.com/)。

    - git 与 gitee 10 分钟就学会，不要看网上那些好几小时的视频教程。

  - 会使用[markdown](https://markdown.com.cn/)来写文档。
    - 半小时就能学会了，是开源界里 99%的文档都是用 markdown 写的。

- 高级技能：将整个网站 git 到本地，在本地编辑并预览

  - 会简单的前端技术：Nodejs React Umi Dumi antDesign Typescript

<img src="./imgs/editpage.jpg" style="zoom: 50%;" />

## 4、小技巧

国内访问 npm 会很慢，经常造成自动编译不成功，所以要在脚本中添加一个镜像

```
sh 'npm config set registry https://registry.npm.taobao.org'
```
