# 调试步进电机电流

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="设置进电机的正确电流量。"
    when="如果您的步进电机的情况下运行良好，您可以跳过此步骤。如果步进电机太热而无法触摸。如果动力系统发生重大变更，例如更换更重的热床，驱动器发生了变化"
    tools="对于新的驱动器：只需要终端软件。如果是老的驱动器：万用表、小螺丝刀和带鳄鱼夹的备用电线（可选但推荐）"
    desc="测试需要25分钟，可以跳过此步骤"
  />
);
```

## 1、背景说明

设置步进驱动器电流是校准 3D 打印机的重要步骤，尽管通常该值不需要精确。

> 经验法则
>
> - 如果步进电机步数不足或您遇到层移位，则需要增加步进电流。这将为电机提供更多扭矩，但也会使其（和驱动器）运行更热。
> - 如果步进电机太热而无法触摸，则需要降低步进电流。这将消除扭矩并使电机（和驱动器）运行更冷。

不幸的是就算增加电流，有时步进电机可能会很热并且仍然缺少步骤。例如以下情况：

- 挤出机步进电机可能存在阻塞。
  - 例如喷嘴部分堵塞、PTFE 管未固定、喷嘴温度过低或喷嘴离热床太靠近（喷嘴卡在床上）
- 对于 X、Y 和 Z，步进电机的尺寸可能与其推动的质量相比过小。
  - 当增加打印机的尺寸
  - 向热床上添加更重的东西（例如玻璃/镜板）
  - 转换电机驱动
- 机械错位使运动变得更加困难。
  - 可能是 V 型滚子太紧
  - Z 轴丝杠未对准，导致 Z 轴被卡住。
- 步进电机的加速度和打印速度太高。
- 每个步进电机驱动器都有一个额定电流，如果这个电流太高，它会运行得非常热，并可能导致漏步。主动冷却可以对此有所帮助，但电流仍应在该驱动器的安全规格范围内。

_如果调整步进驱动器电流无法找到最佳效果，好消息是您可以在大多数情况下轻松升级到更大的步进电机。但您仍应检查您的机器以确保在购买之前有足够的空间安装更长的步进电机。在其他条件相同的情况下，更长的步进电机将能够提供更大的扭矩并处理更高的电流。_

<Alert type="error">

有两种设置电流的方法：

- 物理方式

  - 对于在传统模式下运行的较旧的步进电机驱动器或 TMC 驱动器，通过转动驱动器顶部的微调电位器螺钉来提高或降低 VREF 来设置电流，从而设置驱动器电流。

- 代码方式
  - 在 TMC 驱动程序上，电流直接使用 gcode 命令设置。这可以在固件中、通过终端或使用打印机的 LCD 进行设置。然后应将该值保存到 EEPROM 以保持持久性。

</Alert>

## 2、设置方法

### 2.1 峰值电流和检测电阻值

准确设置步进驱动器电流依赖于知道两个值：步进电机额定峰值电流和步进电机驱动器上的检测电阻值。

对于较新的 TMC 驱动器，检测电阻值是已知的。对于较旧的驱动程序，可以在以下视频中看到确定这一点的方法。

- 为什么 3d 打印机电机会发热，你知道步进电机电流该设置多少合适吗？

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=599958151&bvid=BV1GB4y147Mb&cid=814094045&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

<br/><br/>

- 3D 打印机电流如何调节？电机发热怎么办？

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=468644301&bvid=BV1A541127ZE&cid=589390572&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

### 2.2 物理方法

小心，有可能损坏主板或者驱动。

我之前已经详细介绍过，所以请使用下面的嵌入视频，查看如何设置 VREF。对于任何驱动程序，该过程基本相同。

- 3d 打印机 驱动电流调节教程

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=504279653&bvid=BV1xg411M7kj&cid=375581298&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

VREF 只是帮助我们设置驱动器电流的参考电压。使用它是因为用万用表测量电压而不是电流要简单得多。通常，这些驱动器具有峰值/最大电流设置。

通过 VREF 设置电流的一般步骤在驱动程序之间是相同的，只是 VREF 公式有所不同：

1. 通过 12/24V 普通电源为主板供电，而*不仅仅是*USB 5V。
2. 将万用表设置为直流电压，最大 2V 范围。
3. 将黑色/负极万用表探头接地。这可以是负极端子或 USB 连接器的顶部。
4. 将红色/正极探头连接到驱动器顶部的微调电位器以测量 VREF。
5. 用螺丝刀*慢慢*转动微调罐，然后重新测量。
6. 对每个步进电机驱动器重复此操作。

或者，您可以在红色探头和螺丝刀的金属轴之间使用鳄鱼夹线，以便在转动螺丝刀时获得 VREF 读数。此过程显示在此代码段中：

<img src="./imgs/0-6-djqd.jpg" style="zoom: 67%;" />

测试过的驱动程序的 VREF 公式如下：

#### A4988

典型的检测电阻值为 0.1。请检查您的驱动程序以确定。

```
VREF = 8 x 最大电流 x 检测电阻值
```

#### DRV8825

检测电阻值应为 0.1。如果是：

```
VREF = 最大电流 / 2
```

#### TMC2100

与 gcode 部分中介绍的 TMC 驱动程序一样，TMC2100 的电流不是设置为峰值，而是设置为 RMS。要确定 RMS，请将峰值电流除以**1.41**。

```
VREF = (RMS 电流 * 2.5) / 1.77
```

#### TMC2208 - 传统/独立模式（见 Creality 静音板）

与 gcode 部分中介绍的 TMC 驱动程序一样，TMC2208（传统模式）的电流不是设置为峰值，而是设置为 RMS。要确定 RMS，请将峰值电流除以**1.41**。

```
VREF = (RMS 电流 * 2.5) / 1.77
```

> 一些 Creality 静音板的特别说明
>
> “我的创意静音板有**R150**感应电阻而不是**R100**，所以 VREF 公式是错误的，详情请点击此处”：[EEVBLOG](https://www.eevblog.com/forum/chat/3d-printer-yet/msg3271034/#msg3271034)

#### LV8729

该驱动器主要有两种步进驱动板。

一个在底部有一个标记为 R100 的电阻器，另一个电阻器标记为 R220。你使用的公式是基于这个电阻的

然后，该过程与上面视频中所示的 A4988 基本相同，但为您的驱动板使用了正确的公式。

R100：

```
VREF = 最大电流 / 2
```

R220：

```
VREF = 最大电流 * 1.1
```

### 2.3 代码方法

通过 UART 或 SPI 串行连接的 TMC 驱动程序可以通过 gcode 轻松设置其电流。这不是峰值电流，而是 RMS（均方根）电流。而不是最大值，将其视为更典型/平均电流，驱动器将主要在其中运行。要将峰值电流从步进电机规格转换为 RMS，请将其除以**1.41**。

可以通过几种不同的方式为每个驱动程序设置电流：

#### TMC2208、TMC2209、TMC2130 等

这些驱动器的检测电阻值应为**0.11**。这是 Marlin 中的默认设置，因此在编译时应该已经设置（X 轴为**X_RSENSE ，Y 轴为** **Y_SENSE**，依此类推）：

![](./imgs/tmc1.jpg)

因此，您可以在编译时直接在固件中设置您的 RMS 电流。这是 X 轴的**X_CURRENT ，Y 轴的** **Y_CURRENT**等等。刷新固件后，请记住之前的值可能仍存储在 EEPROM 中。通过在终端中输入**M503**检查您的值。

您也可以通过[M906](https://marlinfw.org/docs/gcode/M906.html)端子设置 RMS 电流。请点击链接查看参考。将 X 轴电流设置为 680 的示例如下：

```
M906 X680
```

之后不要忘记将值保存到 EEPROM：

```
M500
```

最后，LCD **Configuration**菜单可用于设置 RMS 电流。之后不要忘记通过单击**Store Settings**进行保存。

#### TMC5160

TMC5160 与其他 TMC 驱动程序相同，除了一个重要区别：编译固件时需要将检测电阻值从**0.11**更改为**0.075 。**

![](./imgs/tmc2.jpg)

进行此更改后，将应用相同的程序：

您可以在编译时直接在固件中设置 RMS 电流。这是 X 轴的**X_CURRENT ，Y 轴的** **Y_CURRENT**等等。刷新固件后，请记住之前的值可能仍存储在 EEPROM 中。通过在终端中输入**M503**检查您的值。

您也可以通过[M906](https://marlinfw.org/docs/gcode/M906.html)端子设置 RMS 电流。请点击链接查看参考。将 X 轴电流设置为 680 的示例如下：

```
M906 X680
```

之后不要忘记将值保存到 EEPROM：

```
M500
```

最后，LCD **Configuration**菜单可用于设置 RMS 电流。之后不要忘记通过单击**Store Settings**进行保存。
