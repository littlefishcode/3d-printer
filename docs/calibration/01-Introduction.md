# 入门必看

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

这个网站的目的是为了让校准 3D 打印机更加容易。现在网上没有一个系统的校准 3D 打印机的教程，然后就把看到的校验经验进行了汇总主要参考了两个内容：

- [国外一个专门校准的网站](https://teachingtechyt.github.io/)，感觉比较系统，就把调试的过程，一步一步记录下来了。
- [cura 插件 calibration shapes](https://github.com/5axes/Calibration-Shapes)，一个 cura 的插件，感觉不错。

在 B 站上有相关的视频

- [铜牌杰瑞：3D 打印新手必学技能](https://www.bilibili.com/video/BV1jB4y1V7Ss)
- [葫芦~：3d 打印校准](https://www.bilibili.com/video/BV1wL4y1K7aL)

<Alert type="error">

  <div style="font-size:18px ; color:red" >警告 - 请仔细阅读！</div>
  <div style=" color:red" >
    <br/> 本栏目中是针对Marlin打印机进行校验，后来又增加了Klipper打印机的校验放到Voron里面了<br/>
  	<br/>已经尽全力保证gcode的安全性，但是总是存在风险，请在切片软件或<a herf="https://zupfe.velor.ca/">Zupfe GCode</a>中预览，并<b>自行承担打印风险</b>。
  	<br/><br/>  所以需要你在场，以便出现紧急情况可以紧急断电。
  </div>

</Alert>

## 1、网站运行机制

网站最初的 gcode 代码来自 cure，然后使用 Javascript 根据用户设定的参数进行修改。当然你也可以直接在 cure 中进行修改与打印。

## 2、切片参数设置

在 cure4.8 中进行了基本参数的设置

### 2.1 打印机设置

- 基本设置

  - 针对 marlin 固件进行切片

  - 最大打印尺寸：120 x 120 x 160 毫米
  - 有热床

- 挤出机设置

  - 0.5 毫米喷嘴
  - 1.75mm 耗材

### 2.2 具体设置

- [0.5-pla-hot.curaprofile](/curaprofile/0.5-pla-hot.curaprofile)
- [0.5-pla-cool.curaprofile](/curaprofile/0.5-pla-cool.curaprofile)
- [0.5-tpu-cool.curaprofile](/curaprofile/0.5-tpu-cool.curaprofile)
- [0.5-petg-hot.curaprofile](/curaprofile/0.5-petg-hot.curaprofile)

> 下面是 0.5-pla-hot 的配置。 cool 的配置，就把热床的温度设置成 0.

- 质量

  - 层高: 0.25 mm。 这里设置的是喷嘴的 50%
  - 起始层走线宽度：103%。 为了增加底层的附着度

- 外壳

  - 壁走线次数：3
  - 水平扩展：-0.2 。为了补偿过小的空洞。

- 填充

  - 填充密度：30%
  - 填充走线距离：1.6667mm
  - 填充图案：直线

- 材料

  - 打印温度：190 度
  - 打印温度起始层：190 度
  - 起始打印温度：210 度。 防止堵头
  - 最终打印温度：190 度
  - 打印平台温度：60 度

- 速度

  - 打印速度：40

  - 填充速度：40

  - 空驶速度：60

  - 启用加速控制：是

  - 启用抖动速度控制：是

  - ```
    一般情况： 60% 壁的速度 、80% 填充、166% 空驶速度和 50% 的第一层
    ```

- 移动
  - 回抽距离：4mm
  - 回抽速度：45
- 打印平台附着
  - 类型：skirt
  - 走线计数：2

```
0.5-petg-hot的区别
c
水平扩展：0
填充走线距离：1.4286
打印温度：230度
热床：65度
打印速度：30
回抽速度：25

0.5-tpu-cool
起始层走线宽度：110%
填充密度：70%
填充走线距离：0.7143
打印温度：220
热床温度：0
打印速度：30
回抽距离：7mm
回抽速度：25
```

## 3、客户端的安装

如果要调试机器，可以通过机器自带的显示屏来控制。另外也可以通过客户端来控制。

- 本文档主要是通过 web 客户端来控制。
- 当然，你也可以通过[Pronterface](https://www.pronterface.com/)或[Octoprint](https://octoprint.org/)等终端软件来控制。

关于 web 客户端的安装，可以看[这个文档](https://www.littlefish.love/3dprinter/wifi)。通过下面的操作界面，来远程控制打印机。

![](./imgs/wifi-remote-portal.png)

## 4、Calibration Shapes 说明

- [Calibration Shapes 首页](https://github.com/5axes/Calibration-Shapes)

### 4.1、 安装

看下图

<img src="./imgs/calibration-shapes-install.jpg" style="zoom:33%;" />

- [Ultimaker Cura 5/5.1 版本的市场找不到 Calibration Shapes 的解决办法](https://wgznz.com/zhinan/10645-1.html)

### 4.2、使用扩展脚本

具体的使用，在后面会介绍。这里说一下脚本的使用。

如果是低于 4.9 的版本，需要点击下面菜单，然后重启 cura 软件才能生效。

<img src="./imgs/cura-copyScripts.jpg" style="zoom:67%;" />

- 然后会多了 4 个菜单。

<img src="./imgs/cura-Scripts.jpg" style="zoom:50%;" />

- [RetractTower.py](https://github.com/5axes/Calibration-Shapes/blob/master/resources/scripts/RetractTower.py)
- [SpeedTower.py](https://github.com/5axes/Calibration-Shapes/blob/master/resources/scripts/SpeedTower.py)
- [TempFanTower.py](https://github.com/5axes/Calibration-Shapes/blob/master/resources/scripts/TempFanTower.py)
- [FlowTower.py](https://github.com/5axes/Calibration-Shapes/blob/master/resources/scripts/FlowTower.py)

## 5、关于 teachingtechyt

teachingtechyt 是国外一个做校验的人做的网站，下面是他的视频介绍。

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=976288956&bvid=BV1z44y1v7ZL&cid=432031985&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```
