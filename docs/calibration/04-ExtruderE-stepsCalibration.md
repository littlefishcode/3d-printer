# 挤出机校准

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="为了确保挤出机正确的挤出步数。"
    when="挤出机与喷嘴发生变化的时候。"
    tools="尺子、永久记号笔、客户端软件"
    desc="测试需要15分钟，这一步还是比较重要的"
  />
);
```

对于 X、Y 和 Z 轴，每毫米的步数在打印机之间通常是一致的，并且很少随着修改而改变。只要皮带紧实，就很少需要调整。

然而，对于挤出机而言，挤出机硬件和喷嘴的变化意味着值得适当校准每毫米的挤出机步数或 E 步数。

这可以通过客户端发送简单的 gcode 命令来挤出一定数量的耗材，然后测量实际通过系统的耗材数量来完成。

<Alert type="error">

特别说明

- 这种校准最好在挤出机与喷嘴分离的情况下进行，因此对挤出机运动没有限制。如果方便的话，可以拆卸打印机。
- 如果不方便，要用非常缓慢的速度和稍高的温度，来最大减少限制。
- 不能用卫生纸清理喷头，会留残渣的，堵喷头的。
- 打印完毕后别着急关电源，要让风扇吹一会。
- ABS 打印完后，别着急拿出来，等 10 分钟后，封箱空间冷却下来再拿。

</Alert>

## 1、得到当前 E-steps

首先，我们需要知道现有的 E-steps 值。要找到它，请输入：

```
M92
```

例如得到下面的结果

```
M92
echo: M92 X80.16 Y80.00 Z402.41 E97.82
ok
```

如果您只收到一条*ok*消息，或者您可以在输入后查找**M92**行：

```
M503
```

[M92](https://marlinfw.org/docs/gcode/M092.html)用于报告或设置每个轴的每毫米步数。M92 本身会报告当前参数。我们要记下**E**之后的数字，在下面的示例中，**93.00**：

![](./imgs/esteps1.jpg)

## 2、 测量进料尺寸

手动将喷嘴移到床上方足够高的位置，以提供足够的间隙来挤出细丝。

### ① 做 120mm 标记

游标卡尺测量出

<img src="./imgs/jt-jcj-1.jpg" style="zoom:50%;" />

建议使用一个曲别针来进行标记。还有就是剪一根 120mm 的细绳测量，有些便宜的耗材一掰就断，拿卡尺根本无法测量。

如果没有曲别针，用一个便签纸或者颜色笔标记距离。针对黑色的耗材，标记笔不好用。

<img src="./imgs/e-step-test.jpg" style="zoom:80%;" />

### ② 预热喷头

加热喷头正常打印温度高 10 度，假设正常的是 190 度

```unknown
M104 S200 T0
```

获取温度报告 ：M105

### ③ 进料

按照 100mm/分钟的速度，挤出 100mm 耗材

```unknown
T0
G91
G1 E100 F50
G90
```

_系统默认的速度是 400mm/分钟，也就是 F400，这里放慢了进料的速度。_

_[G91](https://marlinfw.org/docs/gcode/G091.html)将打印机置于相对运动模式。这意味着请求 100mm 的耗材会在当前位置增加 100mm，而不是移动到 100mm 的特定位置。_

### ④ 测量实际情况

用游标卡尺测量实际距离。

<img src="./imgs/jt-jcj-2.jpg" style="zoom:50%;" />

理想情况下，剩余 20 毫米，这意味着正好 100 毫米被挤出。如果您的距离不是这个值，请填写下表以计算正确的 E 步数：

_假设上面的结果是 18_

## 3、 算出并保存新的 E-steps

挤出`120mm`来计算出实际得 E-steps

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Estep } from '3d-printer';
export default () => <Estep pMeasurement="20" pOldEstep="84.70" />;
```

## 4、参考文档

### 4.1 参考视频

- 3d 打印机校准 2 挤出机校准

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=642179811&bvid=BV1UY4y1V7D4&cid=736833868&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      />
    </VideoCard>
  );
};
```

### 4.2 补充说明

特别说明：Prusa 在某些打印机（例如 Mini）上禁用了 M500 保存到 EEPROM。在这些情况下，必须将上述 M92 gcode 添加到切片器的起始 gcode 中，以便在每次打印之前读取。

> 双/多挤出的特别说明
>
> 默认情况下，Marlin 预计您的每台挤出机的 e-step 都是相同的。要解决此问题，您必须在**configuration.h**中使用未注释/启用的**DISTINCT_E_FACTORS**进行编译：
>
> ![](./imgs/distinct-e-factors.jpg) 然后，您将能够为每台挤出机输入唯一的 M92 值。
