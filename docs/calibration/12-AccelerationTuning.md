# 校准加速度

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="在打印速度和质量之间找到正确的折衷方案，特别是与重影等表面伪影有关。"
    when="初始校准，当运动系统发生重大变化时（例如，较重的床，远程挤出机更换成进程挤出机。）。"
    tools="本页面上的gocode代码"
    desc="可选步骤，仅适用于特定场景"
  />
);
```

## 1、背景说明

我们在切片机中设置进给速度或移动速度，但打印机不会立即达到这些速度。

就像汽车一样，它需要时间来加速。如果移动的距离很短，它甚至可能来不及达到规定的速度。这可以通过 Prusa 网站上提供的方便的[加速度计算器来确定。](https://blog.prusaprinters.org/calculator/)

作为加速的补充，我们有 jerk，在新版本的 Marlin 中被交叉点偏差所取代。这些设置有所不同，但两者本质上都负责确保打印机不会在每次移动之间完全停止，而是根据下一个“拐角”的角度减速适当的量。

我们将使用另一个塔调整这两个参数。目的是在不引起过度振铃/重影的情况下获得相当快的打印时间。下面是一个不良重影的例子。由于打印机组件的振动，模型的特征在表面上重复出现：

![](./imgs/ghosting.jpg)

我之前就这个主题制作了详细的视频指南，并附有许多解释这些概念的图表。此处描述的调整过程将在此处通过更易于使用的计算器和自定义 gcode 生成器进行改进。

> 经验法则
>
> - 更高的加速度和抖动将导致更快的打印时间，因为打印机更快地达到最高速度并在转弯时保持更高的速度。这对打印机来说更难，并且可能会导致组件寿命缩短和需要更多的定期维护。它还引入了更多的表面缺陷，例如振铃/重影。
>
>   较低的加速度和急动将导致较慢的打印时间，因为打印机会逐渐达到最高速度并以较低的速度转弯。这在打印机上更容易，可能会延长组件的使用寿命并且减少定期维护的需要。它减少了诸如振铃/重影之类的表面伪影，除非它过于保守，在这种情况下，它可能会在角落引入凸起。

## 2、操作步骤

### 2.1 计算最大进给率

这是一个简化的测试，它不如使用[Speed/Max flow 选项卡](https://teachingtechyt.github.io/calibration.html#speed)上的 gcode 生成器准确。如果您想查看它，可以**在此处**切换可见性。

### 2.2 调整加速度

我们现在将制作一个加速塔，以便在一次打印中方便地测试背靠背设置。如果你想自己切片模型，这里是 STL：[accelerationtower.stl](https://teachingtechyt.github.io/files/accelerationtower.stl)。它应该用一个正常的底部切片，但是是空心的，没有顶层，只有 2 个周长。

[具体 gcode 通过这个网站下载](https://teachingtechyt.github.io/calibration.html#accel)
