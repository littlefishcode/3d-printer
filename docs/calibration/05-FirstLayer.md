# 调平校准

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="确保打印机底座水平并与喷嘴保持适当的距离。在使用自动调平的情况下，检查补偿是否有效以及 Z 偏移是否设置正确。这将导致第一层具有正确的“挤压”量，这意味着良好的附着力，并大大增加打印成功的机会。"
    when="第一次安装打印机。重新调整框架、热床、喷嘴等。"
    tools="A4纸、本页面上的gocode代码"
    desc="测试需要15分钟，这一步还是比较重要的"
  />
);
```

## 1、重要性

调平是 3D 打印成功的重要组成部分，并且可能是新用户打印失败的首要原因。

热床和喷嘴之间的垂直距离需要正确，第一层才能正确打印。

- 如果这个距离太远，耗材将无法充分压入床中，甚至可能在半空中打印，并且打印件会从床上脱落并失败。
- 如果喷嘴太靠近，将没有足够的空间让挤出的长丝形成正确的形状，它会被迫向外挤压。在少数情况下，挤出的线会比正常的*宽并产生大象脚*。同时这样的痕迹可能很难从床上去除。
- 如果喷嘴太靠近，在极端情况下，长丝将无法离开喷嘴，可能会堵塞挤出机/热端。

- 对于 **0.25 mm 喷嘴**，我们建议使用**“0.15 mm 质量”**打印设置。
- 对于 0.6 毫米喷嘴，我们建议使用**“0.3 毫米质量”**打印设置。
- 对于 **0.8 mm 喷嘴**，我们建议使用**“0.4 mm 质量”**打印设置。

![](./imgs/first_layer-cropscale.jpg)

> 您可以在 PrusaSlicer 中轻松创建一个矩形，方法是右键单击空的构建板并选择“_添加形状 - >框_”。然后将其缩放到 0.2-0.4 毫米高，具体取决于您要校准的喷嘴。你想要一个只有一层厚的盒子。

![](./imgs/First-Layer-Calibration-02.jpg)

_该图像描绘了 0.4 毫米喷嘴的第一层。这条线的宽度几乎是所描绘宽度的一半。_

## 2、调平

### 2.1 百分表调平

60 元买个百分表做自动调平，从下面的视频上来看，效果非常不错。 视频里面如果统计出各个点位偏差并导出到图表，以及热力图不知道怎么做出来的？

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=672670698&bvid=BV1tU4y1b78i&cid=327676007&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

### 2.2 手工调平

#### ① 回到原点

- 检查喷嘴是否有残余的耗材，如果有，就加热喷嘴，然后擦擦喷嘴
- 确保 z 轴的限位开关在合适的位置。 然后回到原点。

- 检查喷嘴与热床的距离，如果距离过大或者过紧，那么调整限位开关的上下距离。

#### ② 精调距离

用一张 A4 纸进行调平，有摩擦感就行。

热床下的螺母进行调整：

- 顺时针转动，表示减少距离
- 逆时针转动，表示要松一点，喷头离热床太近了。

拿一张 A4 纸进行调平，但是怎么能调的更快呢？

- 网上有人说，先把打印头顶住热床，然后倒转 45%就可以了。

### 2.3 自动调平

现在省略。新手可以刚开始尝试手工调平。今后熟练了，再使用自动调平。

## 3、检验效果-推荐方案 2

推荐用这个方案。因为 calibration shapes 方案中的小方块太小，看不出效果。 这个方案的方块大，效果容易辨别。

### 3.1 打开 stl

[一个 25✕25✕0.3mm 的 stl](/stl/flatsquare.stl)

### 3.2 复制到四周

通过复制来生成四周的效果。

- [这里有一个做好的：0.5-pla-hot-0.25-4-flatsquare](/gcode/0.5-pla-hot-0.25-4-flatsquare.gcode)

![](./imgs/firstlayerpreview.jpg)

### 3.3 打印并查看效果

打印并查看效果。什么是好的调平

- 用手摸顶面，感到光滑。
- 没有喷嘴剐蹭到打印件上面的情况。
- 查看距离
  - 下图和参考图片可用于确定您的第一层是否离喷嘴太近或太远。参考图像非常大以帮助清晰，点击它以最大尺寸查看。
  - 如果一侧看起来太近，而另一侧太远，请调整水平旋钮以纠正此问题。在进行调整以确保结果准确且可重复之后，值得多次打印此 gcode。

<img loading="lazy" src="/firstlayer.jpg"/>

<img loading="lazy" src="/firstlayer2.jpg"/>

## 4、检验效果-方案 2

可以打印出一层看看调平的效果，然后感觉实际打印出来的效果，就知道 A4 摩擦什么程度更好。

将此测试放在其他测试之前，因为假设您的第一层必须合理才能使后面的测试成功。然而，在某些情况下，如果您的流动、回缩等偏离正常，它可能会阻止您的第一层正确粘贴，您可能希望先尝试这些其他测试。

本方案：使用了 cura 的插件 calibration shapes 来检验效果。

### 4.1 添加模型

**添加：a Bed Level Calibration**

<img src="./imgs/calibration-shapes-bed1.jpg" style="zoom:50%;" />

### 4.2 放大尺寸

**通过放缩，让这些方块移动到热床的边缘**

![](./imgs/calibration-shapes-bed2.jpg)

### 4.3 切片打印

按照自己的打印机，[生成了 gcode 文件](/public/gcode/calibration-bed-level-test-0.5-pla-hot-0.25.gocde)

打印时间大概 10 分钟

打印完毕后查看效果。
