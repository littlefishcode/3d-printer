# 调试速度与最大流量

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="针对影响外壁阴影的流量与挤出量的配置，为外壁找到最大的进给率。"
    when="这是一个可选步骤，仅适用于特定场景。例如为tpu耗材找到一个安全的进给率,在尝试更快打印速度时，确定最大可靠进给率，以及调整外壁速度以减少外表面的【噪音】"
    tools="本页面上的gocode代码"
    desc="可选步骤，仅适用于特定场景"
  />
);
```

## 1、背景说明

### 1.1 术语说明

术语说明：这部分比较难懂，所以针对英文的属于做了特殊的约定与说明。

- 进给率或给出率，都表示英文中的“Feed Rate”

- 下面有一段英文的解释

  - M220 - Set Feedrate Percentage Set speed percentage factor, aka “Feed Rate” which applies to all G-code-based moves in all (X, Y, Z, and E) axes. Report the current speed percentage factor if no parameter is specified.
  - ```
    Set feedrate to 80%
    M220 S80
    Report feedrate
    M220
    Back up current feedrate
    M220 B
    ```

我们的 3D 打印机（或任何 CNC 机器）移动的速度更好地描述为“进给率”。与任何速度测量一样，它是对随时间行进的距离的计算。在 3D 打印中，最常见的进给率单位是 mm/sec 和 mm/min。

大多数时候，在我们可以接受的打印质量的前提下，我们希望尽可能快地打印。当然有些人说，如果用更高的速度进行打印，会损害打印机，这种说法可能针对哪些入门的 dir 打印机，毕竟时间还是比较重要的。

### 1.2 进给率的影响

60-100 打印速度中对拐角的影响

![](./imgs/feedrate-1.jpg)

20-60 的打印速度

![](./imgs/feedrate-2.jpg)

用线性推进发现拐角问题解决了，但是出现外壁阴影

![](./imgs/feedrate-3.jpg)

换个图看看

![](./imgs/speedartefacts.jpg)

### 1.3 机器限制

来自挤出机驱动器和热端的进料速度限制。

找到你的 3D 打印机最大的进给率，这个给出率可以保证可靠的挤压，这与耗材通过热端融化的速度有关。一旦耗材不能充分熔化，它就不能正确地流过系统，并且会在挤出或堵塞的情况下发生。

有时加热器的问题，而是来自挤出机驱动器的抓握和推动量。使用柔性耗材，例如 TPU。TPU 不需要特别高温的热端来正确熔化它，但许多挤出机驱动器很难抓住并将耗材推向热端，在这些情况下，可以使用此页面上的测试来确定在系统发生故障之前您可以打印多快。

### 1.4 关于切片软件

了解切片机软件如何处理进给率非常重要。

切片机通常具有大多数速度计算所基于的默认/基本进给率。周长、外周长和实心填充等特征的速度会降低，以提高视觉打印质量。诸如内部填充之类的功能被加速，因为它们永远不会被看到。下图显示了几个切片器中的示例接口：

![](./imgs/slicerfeedratemodifiers.jpg)

例如图中 cura 中速度是 50，外壁的速度是 25。

### 1.5 测试理念- M220

通过[speedtower.stl](/stl/speedtower.stl)来测试进给率。如果用 40 的速度打印，需要 2 个小时。

<img src="./imgs/feedrateTower.jpg" style="zoom:50%;" />

该进给率测试塔以花瓶模式切片。这有几个原因：

- 在第一层之后，花瓶模式打印只有一种特征：外部周边。这清楚地表明了塔的每个部分的实际进给率。
- 花瓶模式在基础填充物完成后会持续挤出，因此不需要缩回。这意味着我们不会被撤回和层更改引入的伪影所迷惑。
- 实心底层完成后，花瓶模式打印没有任何移动。这意味着我们可以增加打印进给率，而不必担心移动进给率会按比例增加到危险值。
- 由于上述原因，花瓶模式非常适合使用**M220**进给率修改器 gcode 来更改每个测试段的进给率。这在[Marlin](https://marlinfw.org/docs/gcode/M220.html)、[Klipper](https://www.klipper3d.org/G-Codes.html)和[RepRapFirmware](https://duet3d.dozuki.com/Wiki/M220)中得到支持。从打印机的 LCD 控件或 Web 界面更改进给速度中间打印使用固件的此功能。此测试会将**M220**命令插入 gcode 以动态更改速度。

M220 可以是一把双刃剑，因为它也会影响回缩动作的速度。如果您正在执行诸如回缩测试之类的操作，则 M220 不适合。然而，在这种情况下，由于使用了花瓶模式，M220 是一种简单的方法，我们可以从较慢的进给速度开始，并在整个打印过程中以设定的高度间隔轻松增加进给速度。

## 2、具体测试

### 2.1 找到最大限制

找到最大进给率和加速度限制。

每个固件都有一个安全功能，可以将进给率和加速度限制为用户设置的最大值。这可以防止切片器或终端命令中的拼写错误导致移动速度过快而损坏打印机。使用以下说明检查固件的进给率/加速度限制，并在必要时暂时提高它们。

请注意，对于此测试，您通常只会更改进给率限制。为了完整起见，此处还涵盖了加速限制。

[Marlin 使用 M203](https://marlinfw.org/docs/gcode/M203.html) 限制最大进给率。

您可以在终端中输入**M203**，然后将报告最大设定进给速度。如果您希望提高这些限制，请重新输入**203**并使用**X**和**Y**参数设置最大进给率，通常以毫米/秒为单位。例如：

```
M203 X500 Y500
```

其中 500 是 500 毫米/秒的进给率。

Marlin 使用[M201](https://marlinfw.org/docs/gcode/M201.html)和[M204](https://marlinfw.org/docs/gcode/M204.html) gcode 限制加速度。

您可以将**M503**输入终端，查看机器中的这些值。

**M204**设置打印、缩回和行进移动的目标加速度，但**M201**值对每个轴施加限制。例如，如果您将**M204**打印加速度设置为 1000，但**M201 Y**加速度限制仅为 800，则打印时 Y 的**M204 P**值将上限为 800。如果您有一个 y 轴较重的床吊具，则可以通过这种方式使用 M201 来对抗抖动。

如果您希望此更改是永久性的，请使用[M500](https://marlinfw.org/docs/gcode/M500.html)保存到 EEPROM 。

### 2.2 生成 gcode

[可以通过这个网站生成 gcode](https://teachingtechyt.github.io/calibration.html#speed)

下面要给出一个基本概念

#### 进给率/速度

默认打印速度修改为周长为 100%，移动为 166%，第一层为 50%。对于段 A，生成的 gcode 将使用这些比例进行修改（**计算的进给率以灰色显示**）。请注意，挤出机回缩/非回缩和 Z-hop 速度将不受此影响。

在此测试中，您输入的进给率是针对单个外周长的。为 A 段选择一个安全的进给率，以确保与床身有良好的附着力。根据您的喜好增加 B 到 E 段的进给率。由于此打印是在花瓶模式下完成的，因此没有**回抽**。

本网站使用 1.2 x 喷嘴宽度的挤出宽度。使用该宽度、层高和进给率计算体积流量值。该值可用于获得给定温度下热端体积流量的实用概念。

#### 设置每层速度

可以设置每层的设置，并且下载 gcode

![](./imgs/feedrate-setting.jpg)

测试的效果，可以来确定柔性耗材的内容。

![](./imgs/feedrate-pla.jpg)

### 2.3 逆向推导

一些切片软件支持使用上面计算的体积流量作为硬限制。对于那些不这样做的人，您可以使用以下计算器对您首选的挤出宽度和层高的最大进给量进行逆向工程：

![](./imgs/feedrate-r.jpg)

## 3、参考视频

如果对本节的概念有疑问，这里有一个视频可以进行说明

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=555190549&bvid=BV1wv4y137dE&cid=753472048&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      ></iframe>
    </VideoCard>
  );
};
```
