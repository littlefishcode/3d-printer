# 调试线性步进值

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="调整挤压的时间，目的是减少肿胀的角落和更薄的壁。这导致更一致的挤出和表面伪影的减少。"
    when="初始校准，在更换挤出机/热端时（例如，远程挤出机更换成进程挤出机。）。"
    tools="Marlin 线性前进模式发生器:https://marlinfw.org/tools/lin_advance/k-factor.html"
    desc="可选步骤"
  />
);
```

## 1、背景说明

在 3D 打印机中，由于将熔化的细丝推动通过喷嘴的小开口所需的压力，因此从挤出机推动细丝到实际从喷嘴出来之间存在一小段时间延迟。传统上，挤出机的运动与打印机的 XY 运动相匹配，因此这意味着一条线的起点将欠挤压，而线的末端将过度挤压。线性推进使挤出机运动与 XY 运动不同步，改变了挤出机的时间，从而显着减少了薄和厚部分。

> ##### 特别说明
>
> - 线性推进通常被称为压力推进。他们是一样的东西。
>
>   默认情况下，Marlin 固件中通常不启用线性前进。因此，固件必须重新编译，包括线性推进。这在上面的视频中有介绍。
>
>   线性前进与某些步进电机驱动器不兼容。一个突出的是在传统模式下连接时的 TMC2208（如在 Creality 静音板上找到的）。当通过 UART 以“智能”模式连接时，这不是问题。
>
>   线性推进当前与 S 曲线加速（另一个 Marlin 功能）不兼容，尽管在添加线性推进作为解决方法时可以取消注释**#define EXPERIMENTAL_SCURVE 。**
>
>   线性推进需要挤出机的积极加速，并且会使电机更加努力。E 驱动器可能需要更高的电流，这将使其运行更热。
>
>   线性推进取决于耗材。每个耗材需要不同的值才能获得最佳效果。
>
>   线性推进测试依赖于单层的目视检查，因此重要的是让您的床平整/第一层可靠且可重复。

## 2、模型生成器

Marlin 拥有出色的[线性高级文档](https://marlinfw.org/docs/features/lin_advance.html)和已经制作的测试 gcode 生成器，因此在这里重新创建竞争对手是没有意义的。上面的视频中显示了如何使用它的示例，可以在此处找到：[Marlin Linear Advance Pattern Generator](https://marlinfw.org/tools/lin_advance/k-factor.html)

我们为线性推进调整的参数称为**K 因子**。K 系数与长丝中的弯曲或压缩量以及挤出机和热端之间的路径长度有关。

较高的 K 值适合近程挤出机和/或柔性细丝。这是因为细丝可以在挤出机和热端之间的管中横向弯曲，从而增加挤出时间延迟。鲍登挤出机的一个良好起点是 K 值为**1.0**。

较低的 K 值适合直接驱动的挤出机和更硬的长丝。凭借这些特性，挤出机和热端之间的长丝传输更直接，延迟更短。直接驱动挤出机的良好起点是**0.2**。

以上视频带你了解如何使用 pattern generator，主要是输入打印机和切片机参数，然后点击下载 gcode 文件。

使用上面建议的起始 K 值，然后您可以在此值的任一侧选择一个上限和下限进行初步测试。

![](./imgs/patterngenerator.jpg)

## 3、解释结果

打印模式生成器生成的 gcode 并产生如下结果：

![](./imgs/linearadvanceresults.jpg)

有些水平线应该有明显的粗细部分，有些甚至可能有很大的间隙。您正在寻找从左到右挤出宽度最一致的线。该行的 K 值将打印在该行的右侧。此时，如视频所示，您可能希望在此最佳 K 值的任一侧使用更窄范围的值重复测试。这将有助于通过使用“更高分辨率”来确定最佳值。

## 4、保存 K 因子

到目前为止，我们已经调整了许多参数，我们可以将它们永久保存到固件或 EEPROM 中。由于线性前进 K 系数取决于耗材，因此如果您使用不同的耗材进行打印，这可能不是最佳解决方案，您可能更愿意使用切片机配置文件进行保存。下面介绍了所有方法。

可以使用[M900](https://marlinfw.org/docs/gcode/M900.html) gcode 设置 K 因子：

```
M900 K0.11
```

可以通过以下方式永久存储在 EEPROM 中：

```
M500
```

K 因子的设置和保存也可以使用 LCD 菜单实现。

您可能更喜欢在您的开始 gcode 中使用**M900** gcode 命令，特别是如果您的切片器支持不同材料的不同开始 gcode。如果您使用启动 gcode，除非后面有**M500** ，否则 K 因子的设置将是临时的。下次重新启动打印机时，将恢复存储在 EEPROM 中的值。当新打印开始给定它的值时，它的开始 gcode 将覆盖先前设置的值。

通过将 K 因子设置为 0，可以暂时禁用线性前进：

```
M900 K0
```

## 5、参考视频

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=506260989&bvid=BV19u411d7Gh&cid=432037526&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```
