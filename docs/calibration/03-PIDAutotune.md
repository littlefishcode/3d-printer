# PID 自动校准

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="确保喷嘴和热床的加热安全、稳定、一致。"
    when="更换喷嘴，例如更改冷却风扇、管道。更换热床，例如添加玻璃、磁弹簧钢板或床下绝缘材料。"
    tools="客户端软件"
    desc="测试需要20分钟，如果是整机出厂不用测试，但是更换了风扇，喷嘴，热床等，需要测试。"
  />
);
```

[什么是 PID？给你讲个故事，通俗易懂！](https://zhuanlan.zhihu.com/p/314001071)

## 1. 校准方法

PID 自动调整既快速又简单。在 Marlin 中，这是一个使用[M303](https://marlinfw.org/docs/gcode/M303.html)的非常简单的过程。看下面的内容就行。

```
M503 ; 查看当前的系统参数，重点看M303,并记录下来
M106 S255  ;冷却风扇设置为您的正常速度,本示例为 100% 风扇速度
M303 E0 S230 U1 ; 将热端调整为 230度为了打印ABS,可以更改S值,U1表示将结果存储到 RAM 中
M500 ; 将其保存到 EEPROM
M107 ; 关闭风扇
M303 E-1 S90 U1  ; 将热床调整为 90 度
M500 ; 将其保存到 EEPROM

```

如果打印 PLA 耗材，那么喷嘴温度在 190，热床在 60

## 2、G 代码说明

①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳

本节是解释说明部分，可以不看。

### ① M503

查看原始的结果是：

```
M301 P17.05 I0.55 D225.05  ;Set Hotend PID
M304 P480.34 I76.65 D805.85  ;Set Bed PID
或者输出
echo:  M301 P25.43 I1.59 D101.95
echo:  M304 P127.88 I24.52 D444.57
```

### ② M106 S255 开始风扇

### ③ M303 E0 S190 U1 校准

执行后喷嘴`M303 E0 S230 U1 ;`显示的内容如下：

```
bias: 165 d: 89 min: 186.93 max: 193.66 Ku: 33.64 Tu: 38.24
 Classic PID
 Kp: 20.18 Ki: 1.06 Kd: 96.46
PID Autotune finished! Put the last Kp, Ki and Kd constants from below into Configuration.h
#define DEFAULT_Kp 20.18
#define DEFAULT_Ki 1.06
#define DEFAULT_Kd 96.46
ok
```

这将在 230 度时调谐热端。可以更改 **S** 值以适合最常见的打印温度。**U1** 表示结果存储到 RAM 中，我们可以通过发送以下内容立即将其保存到 EEPROM：

```
M500
echo:Settings Stored (690 bytes; crc 50433)
ok
```

> 特别提示：如果您的打印机不支持在 EEPROM 中保存设置
>
> - 使用[M301](https://marlinfw.org/docs/gcode/M301.html)与 [M304](https://marlinfw.org/docs/gcode/M304.html)手工设置。

### ④ M107 关闭风扇

如果不是静音风扇，那么就关闭吧。

### ⑤ 调热床 M303 E-1 S60 U1

```
 bias: 93 d: 93 min: 59.79 max: 60.16 Ku: 639.42 Tu: 10.43
 No overshoot
 Kp: 127.88 Ki: 24.52 Kd: 444.57
PID Autotune finished! Put the last Kp, Ki and Kd constants from below into Configuration.h
#define DEFAULT_bedKp 127.88
#define DEFAULT_bedKi 24.52
#define DEFAULT_bedKd 444.57
ok
```

床选择**E-1**，温度设置为 60 度。根据需要替代正常的打印床温度。之后再次保存到 EEPROM，使用：` M500`

```
M500
echo:Settings Stored (690 bytes; crc 50642)
ok
```

## 3. 参考视频

一个关于失温的视频

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=855721221&bvid=BV1LV4y1n7dd&cid=766174034&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      ></iframe>
    </VideoCard>
  );
};
```

3D 打印机校准 5 打印头、热床 PID 校准

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=597940373&bvid=BV1gB4y1B7gC&cid=760025710&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      />
    </VideoCard>
  );
};
```
