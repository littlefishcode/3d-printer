# 切片软件流量校准

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

本次测试也成为基线测试，今后如果做了其他的调整，可以以次为基线，进行对比测试。

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="针对不同的切片软件，确定3D打印机挤出的正确数量的耗材。"
    when="基础校准，以及挤出机/喷嘴发生变化。您可能希望在调整线性推进后重新进行校准。"
    tools="切片软件cura,游标卡尺"
    desc="测试需要20分钟，这一步还是比较重要的"
  />
);
```

我们将 E=steps 校准正确后，现在将校准切片软件。每个切片软件都有一个设置，来控制打印机耗材的挤出量。如果流量增加，更多的耗材被挤出，如果流量降低，挤出的耗材将减少。

在` Simplify3D` 和` PrusaSlicer` 中，这称为**Extrusion Multiplier**。`Cura` 称之为**Flow**。

## 1、打印薄壁立方体

校准正确流量的方法是打印一个具有指定壁厚的空心单壁立方体，然后测量壁的实际厚度并调整切片机中的流量以适应。

这里通过打印一个单壁立方体来进行测试。 有人会问，为什么不打印一个多壁立方体呢？ 例如挤出宽度 0.4mm，向打印两圈，那么测量壁厚为 0.8 可以不？ 但是这样会引入跟多的变量，例如周边重叠的数量，可以会影响测试结果。

这就是为什么我个人更喜欢单壁立方体。

具体步骤如下：

| 1.导入 STL | [cube.stl](/stl/cube.stl) |
| --- | --- |
| 2.关闭填充 | 填充 > 填充密度：0% |
| 3.关闭顶层 | 外壳 > 顶部/底部厚度 > 顶层厚度 > 顶部层数：0 |
| 4. 确保壁厚为已知值。<br/>在此处替换您喜欢的任何值。<br/>此示例使用**0.5** | 外壳 > 壁厚：**0.5** `实际是0.8不能修改，这里是能是假设成喷嘴厚度` |
| 5.设置外壁厚度为单挤压 | 外壳 > 壁厚 > 壁走线次数：1 |
| 6. 将流量设置为默认值：1.0 / 100% | 材料 > 流量：100 & 材料 > 起始层流量：100（第一层流量） |
| 7.生成 gcode | [0.5-pla-hot-0.25-cube.gcode](/gcode/0.5-pla-hot-0.25-cube.gcode) |
| 8. 预期结果： | ![](./imgs/curacube.jpg) |
|  |  |

> 特别说明：
>
> - 其他一些因素可能会影响结果的准确性。
> - 有些打印机会减慢进给速度并改变壁厚。有些会因为冷却系统不足，外壁非常热并变形。
> - 为了克服这个问题，您可以放大立方体的 X 和 Y 维度。壁厚不会因这种比例变化而改变，测试有效。

## 2、测量外壁厚度

使用数字/游标卡尺测量空心立方体的外壁厚度。在多个位置/侧面进行测量并取平均值。

<img src="./imgs/measurecube.jpg" style="zoom: 33%;" />

如果您的测量值*明显*偏离，则可以使用以下计算器来计算新的流量：

## 3、计算流量并调整

```jsx
/**
 * inline: true
 */
import React from 'react';
import { FlowRate } from '3d-printer';
export default () => (
  <FlowRate pOldFlowRate="100" pTargetThickness="0.5" pMeasuredThickness="0.6" />
);
```

> 重要的提示
>
> - 实践比理论计算更重要。执行此校准后，请根据您实际看到的情况将流速调高或调低。
> - 例如 打印一下立方体
>   - <img src="./imgs/xyzcube.jpg" style="zoom: 33%;" />
>   - 例如上面打印的作品，显示出明显的挤压不足迹象。顶部填充物以及周边和填充物之间存在间隙。不管校准程序如何确定，该切片机/打印机组合的流速都需要增加。

## 4、喷嘴大小的选择

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=676309813&bvid=BV1sU4y1c7rK&cid=430041906&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```
