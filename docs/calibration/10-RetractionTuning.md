# 调试回抽

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="主要是为了校准回抽相关的参数：回抽距离、回抽速度、额外填充距离、额外填充速度、Z轴抬起距离"
    when="在尝试新类型、品牌的耗材时，更换热端或挤出机时进行初始校准。"
    tools="本页面提供的Gcode 生成器"
    desc="测试需要40分钟，这个还是比较重要的"
  />
);
```

**针对一些入门级的打印机，可以忽略这一步。**

[google 的视频教程](https://3dprinterly.com/how-to-use-cura-post-processing-scripts-change-filament-more/)

## 1、回抽校准(基本)

本节使用[Calibration Shapes 首页](https://github.com/5axes/Calibration-Shapes)测试回抽，因为使用这个最简单，只校准距离与速度两个参数。

我现在使用的机器是 0.5mm 喷嘴，0.25mm 切片。默认回抽参数是：

- 回抽距离：4mm
- 回抽速度：45mm/s

切片，观察本次次测试大概 24 到 30 分钟。同时得到每部分大概的层数：

- 1 部分：4-32
- 2 部分：33-62
- 3 部分：63-93
- 4 部分：94-124
- 5 部分：125-152

这个模型比较奇怪，但是可以设置在层高 2.5mm 情况下，每部分间隔是 29。

### 1.1 测试回抽距离

大概需要 60 分钟，如果测试两种情况的话。

由于我的打印机是近端挤出，近端挤出推荐是 1mm 回抽。所以我设置从 1mm 回抽开始，测试这个打印机推荐的 4mm 回抽是否合理。如果是远程挤出的打印机，可以从 5mm 基数进行测试。

<img src="./imgs/retractTower-distance.jpg" style="zoom: 50%;" />

也有人推荐使用：近程挤出机，回缩距离从 0.4 到 1.4 毫米不等，增量为 0.2 毫米。

如果按照这个，那么要按照下面的设置来进行计算：

<img src="./imgs/retractDistance2.jpg" style="zoom:50%;" />

观察打印过程，如果在外层出现斑点，打印机可能打印出了过多的耗材。可以通过调试额外填充距离来解决这个问题。

### 1.2 测试回抽速度

大概需要 30 分钟。

在测试回抽距离完美的情况下，测试回抽速度。当前默认的回抽速度是 45mm/s，按照+-5 来进行测试。：35 40 45 50 55

```
这里F2100 是 2100/60 = 35mm/s 速度
G1 F2100 E154.53188
```

打印完毕后，看看打印件，那个效果最好。

## 2、校准回抽(高级)

上面只校准了关键的参数。本节可以校准更复杂的参数。

- 回抽距离
- 回抽速度
- 回抽装填速度
- 回抽额外装填量
- 回抽 z 轴抬起距离

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=897263981&bvid=BV1eA4y1o7Sg&cid=741431027&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

### 2.1 切片

使用 cura 对[retractiontestv2.stl](/stl/retractiontestv2.stl)进行切片，生成 gcode 文件，然后使用下面的工具进行操作。

### 2.2 添加回抽代码

### 2.3 检查打印件

下面举例对于进、近程挤出机，回缩距离从 0.4 到 1.4 毫米不等，增量为 0.2 毫米。段 A 和 B 的拉弦最少。基于此，我假设 0.4 - 0.6 的缩回距离最适合这台打印机。

![](./imgs/retractionresults.jpg)

接下来，可以固定回收距离，然后通过改变回收速度来进行测试。

然后可以按照回抽额外装填量与 Z 轴提升速度进行测试。

## 3、原理说明

### 3.1 拉丝的原因

FDM 的工作原理是熔化塑料细丝并一次精确地挤出一层以构建 3D 几何图形。就其性质而言，即使没有被挤出机推动，塑料也会继续从喷嘴中渗出和滴落。

为了解决这个问题，我们的切片机使用回缩，将细丝从热端抽出，减轻压力并最大限度地减少渗出。当适当调整时，这具有消除拉线的效果，即模型两点之间不需要的塑料渗出。

下图中可以看到拉丝的示例。它看起来像蜘蛛网：

<img src="./imgs/stringing.jpg" style="zoom:67%;" />

> 特别说明：
>
> - 温度调整和回缩调整相互关联。您可以按任意顺序进行操作，并且可能需要来回调整以达到理想结果。较高的喷嘴温度将促进更多的渗出和拉丝，而较低的温度将减少渗出和拉丝。
> - 有没有在同一个模型中，把温度与回抽全部测试出来呢？

### 3.2 回抽的相关参数

除了喷嘴温度，我们还将调整与回抽相关的五个参数。表格中列出了 cura 中的设置位置（其他切片软件今后补充）。到目前为止，最重要的是**缩回距离**。

| 回抽参数 | 说明 | 我的默认值 |
| --- | --- | --- |
| 回抽距离 | 耗材被拉扯的距离，mm | 4 |
| 回抽速度 | 耗材回抽速度，mm/秒 | 45 |
| 回抽装填速度 | 耗材重新引入喷嘴的速度，以毫米/秒为单位。一般与回抽速度一样 | 45 |
| 回抽额外装填量 | 当非挤压结束时，回抽距离会被反转。这个参数经常设置成 0。当然设置为正数，会挤出额外的耗材。 设置成负数，会进一步回抽耗材。 | 0 |
| 回抽 z 轴抬起距离 | 在行程（非挤压）运动期间喷嘴垂直提升的量（以 mm 为单位）。在此移动之后，然后在耗材未缩回/再次准备打印之前恢复正确的 Z 值。 | 不启用 |
|  |  |  |

![](./imgs/huichoupara.jpg)

## 4、 代码分析

本节比较抽象，不建议看。

### 4.1 cura 切片分析

先用 cura 切片，希望得到如下结果：

- 每部分的间隔。
- 四种关键参数在 cura 中的代码应用。

> 每部分的间隔层数:20 层

```
分别从
A 21
B 41
C 61
D 81
E 101
```

> cura 启用回抽与不启用的区别

首先在第一层前面进行了回抽，回抽了-4mm，后面就再也没有回抽。2700/60=45 是设置的回抽速度。

```
G1 F2700 E-4
;LAYER_COUNT:119
;LAYER:0
.........
G1 F2700 E0  ;再经过几行代码后，把E设置成0，这里是打印裙边。

```

看下面的代码

![](./imgs/retration-diff-1.jpg)

```
M204 S2000  ;空驶加速度 2000
M204 S500   ;  打印外壁等用到的加速度

M205 X30 Y30 ;空驶抖动速度
M205 X10 Y10 ;设置第一层，以及抖动的最大打印速度
```

> M204 - 设置启动加速度
>
> - [P<accel>] 打印加速。用于包括拉伸的移动（即使用当前工具）。
> - [R<accel>] 缩回加速度。用于挤出机回缩动作。
> - [S<accel>] 移动加速度的传统参数。设置打印和行进加速。
> - [T<accel>] 行驶加速度。用于不包含拉伸的移动。

下面这个比较复杂

> [M205](https://marlinfw.org/docs/gcode/M205.html) - 设置高级设置: 这里只描述 xy 参数，实际上的参数更多。
>
> - 设置各种运动设置。使用 查看当前设置[`M503`](https://marlinfw.org/docs/gcode/M503.html)。如果`EEPROM_SETTINGS`启用，则使用 保存[`M500`](https://marlinfw.org/docs/gcode/M500.html)、加载[`M501`](https://marlinfw.org/docs/gcode/M501.html)并使用 重置[`M502`](https://marlinfw.org/docs/gcode/M502.html)。
> - [X<jerk>] X 最大加加速度（单位/秒）
> - [Y<jerk>] Y max jerk (units/s)

### 4.2 RetractTower.py 代码分析

虽然没学过 python 代码，但是看到这段代码，发现 python 的语法很简单，也很容易懂。下面记录了一下具体的代码逻辑。

看着就手痒，想着是不是把这个代码翻译成 javascript，这样就可以放到网页上运行了。当然现在网上有自动的转换工具[关于将 Python 3.5 和更高版本的代码转换为 JavaScript](https://github.com/metapensiero/metapensiero.pj)，好奇在 github 上，还发现了[js 转 pathon 有 2k 的项目](https://github.com/PiotrDabkowski/Js2Py)

①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳

#### ① 公共函数

is_begin_layer_line

### 4.2 teachingtechYT 代码解析

`gcodeprocessing.js`文件中

> 240 行：得到用户输入信息
>
> - 速度都乘上了 60

```js
    // collect retraction inputs
    if(name == "retractionForm") {
        var a1 = formName.ret_a1.value;
        var a2 = formName.ret_a2.value*60;
        var a3 = formName.ret_a3.value;
        var a4 = formName.ret_a4.value*60;
        var a5 = formName.ret_a5.value;
        var b1 = formName.ret_b1.value;
        var b2 = formName.ret_b2.value*60;
        var b3 = formName.ret_b3.value;
        var b4 = formName.ret_b4.value*60;
        var b5 = formName.ret_b5.value;
        var c1 = formName.ret_c1.value;
        var c2 = formName.ret_c2.value*60;
        var c3 = formName.ret_c3.value;
        var c4 = formName.ret_c4.value*60;
        var c5 = formName.ret_c5.value;
        var d1 = formName.ret_d1.value;
        var d2 = formName.ret_d2.value*60;
        var d3 = formName.ret_d3.value;
        var d4 = formName.ret_d4.value*60;
        var d5 = formName.ret_d5.value;
        var e1 = formName.ret_e1.value;
        var e2 = formName.ret_e2.value*60;
        var e3 = formName.ret_e3.value;
        var e4 = formName.ret_e4.value*60;
        var e5 = formName.ret_e5.value;
        var f1 = formName.ret_f1.value;
        var f2 = formName.ret_f2.value*60;
        var f3 = formName.ret_f3.value;
        var f4 = formName.ret_f4.value*60;
        var f5 = formName.ret_f5.value;
```

> 629 行：对代码进行了替换

```js
// process user retraction
if (name == 'retractionForm') {
  // A section
  gcode = gcode.replace(/;retract1\nG1 Z[0-9\.]+ F1200/g, ';retract1\n;zhop1');
  if (a5 > 0) {
    gcode = gcode.replace(/;zhop1/g, 'G91\nG1 Z' + a5 + ' F1200 ; custom z hop - A\nG90');
  }
  gcode = gcode.replace(/;retract1/g, 'G1 E-' + a1 + ' F' + a2 + ' ; custom retraction - A');
  gcode = gcode.replace(
    /;unretract1/g,
    'G1 E' + a3 + ' F' + a4 + ' ; custom un-retraction/prime - A',
  );
  // B section
  gcode = gcode.replace(/;retract2\nG1 Z[0-9\.]+ F1200/g, ';retract2\n;zhop2');
  if (b5 > 0) {
    gcode = gcode.replace(/;zhop2/g, 'G91\nG1 Z' + b5 + ' F1200 ; custom z hop - B\nG90');
  }
  gcode = gcode.replace(/;retract2/g, 'G1 E-' + b1 + ' F' + b2 + ' ; custom retraction - B');
  gcode = gcode.replace(
    /;unretract2/g,
    'G1 E' + b3 + ' F' + b4 + ' ; custom un-retraction/prime - B',
  );
  // C section
  gcode = gcode.replace(/;retract3\nG1 Z[0-9\.]+ F1200/g, ';retract3\n;zhop3');
  if (c5 > 0) {
    gcode = gcode.replace(/;zhop3/g, 'G91\nG1 Z' + c5 + ' F1200 ; custom z hop - C\nG90');
  }
  gcode = gcode.replace(/;retract3/g, 'G1 E-' + c1 + ' F' + c2 + ' ; custom retraction - C');
  gcode = gcode.replace(
    /;unretract3/g,
    'G1 E' + c3 + ' F' + c4 + ' ; custom un-retraction/prime - C',
  );
  // D section
  gcode = gcode.replace(/;retract4\nG1 Z[0-9\.]+ F1200/g, ';retract4\n;zhop4');
  if (d5 > 0) {
    gcode = gcode.replace(/;zhop4/g, 'G91\nG1 Z' + d5 + ' F1200 ; custom z hop - D\nG90');
  }
  gcode = gcode.replace(/;retract4/g, 'G1 E-' + d1 + ' F' + d2 + ' ; custom retraction - D');
  gcode = gcode.replace(
    /;unretract4/g,
    'G1 E' + d3 + ' F' + d4 + ' ; custom un-retraction/prime - D',
  );
  // E section
  gcode = gcode.replace(/;retract5\nG1 Z[0-9\.]+ F1200/g, ';retract5\n;zhop5');
  if (e5 > 0) {
    gcode = gcode.replace(/;zhop5/g, 'G91\nG1 Z' + e5 + ' F1200 ; custom z hop - E\nG90');
  }
  gcode = gcode.replace(/;retract5/g, 'G1 E-' + e1 + ' F' + e2 + ' ; custom retraction - E');
  gcode = gcode.replace(
    /;unretract5/g,
    'G1 E' + e3 + ' F' + e4 + ' ; custom un-retraction/prime - E',
  );
  // F section
  gcode = gcode.replace(/;retract6\nG1 Z[0-9\.]+ F1200/g, ';retract6\n;zhop6');
  if (f5 > 0) {
    gcode = gcode.replace(/;zhop6/g, 'G91\nG1 Z' + f5 + ' F1200 ; custom z hop - F\nG90');
  }
  gcode = gcode.replace(/;retract6/g, 'G1 E-' + f1 + ' F' + f2 + ' ; custom retraction - F');
  gcode = gcode.replace(
    /;unretract6/g,
    'G1 E' + f3 + ' F' + f4 + ' ; custom un-retraction/prime - F',
  );
} else {
  gcode = gcode.replace(/;retract1\nG1 Z[0-9\.]+ F1200/g, ';retract1\n;zhop1');
  if (zhop > 0) {
    gcode = gcode.replace(/;zhop1/g, 'G91;\nG1 Z' + zhop + ' F1200; custom z hop\nG90;');
  }
  gcode = gcode.replace(/;retract1/g, 'G1 E-' + retDist + ' F' + retSpeed + ' ; custom retraction');
  gcode = gcode.replace(
    /;unretract1/g,
    'G1 E' + retDistExtra + ' F' + retSpeed + ' ; custom un-retraction/prime',
  );
}
```
