# 方块校准

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

本次测试也成为基线测试，今后如果做了其他的调整，可以以次为基线，进行对比测试。

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="建立基线以与以后的测试或修改之前进行比较。"
    when="在进行一般校准之前或在进行重大修改之前进行。"
    tools="切片软件"
    desc="测试需要25分钟，这一步还是比较重要的"
  />
);
```

在实际过程中，要打印很多次方块，打印多这么方块，会不会浪费呀，所以我做了一个骰子，除了用户 3D 打印机校验，还可以平时玩个游戏。

方块打印速度很快，并且可以很好地指示打印机是否存在任何基本问题。

- [calibrationCube.stl](/stl/calibrationCube.stl)
- [calibrationCube-0.5-pla-hot-0.25.gcode](/gcode/calibrationCube-0.5-pla-hot-0.25.gcode)

![](./imgs/calibrationCube.jpg)

如果没有大问题，请继续下一步。如果存在重大缺陷，则很可能通过检查框架选项卡找到罪魁祸首。小问题有望在后续测试中得到解决。

如果不想用我做的方块，可以[通过 iDig3Dprinting 创建 XYZ 20mm 校准立方体](https://www.thingiverse.com/thing:1278865)的定制版本。
