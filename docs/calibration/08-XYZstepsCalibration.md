# XYZ 轴步长校准

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="确保xyz轴精度"
    when="第一组装机器，更换皮带等,定期检查皮带松紧时，紧皮带时。"
    tools="游标卡尺或百分表"
    desc="打印50mm方块，测试需要2小时。如果用百分表测量，大概需要20分钟。也可以忽略这个测试"
  />
);
```

如果想要精确校准，需要一个百分表，这个 60 元左右，千分表太贵。**还需要额外做一些工具。所以针对入门级别的打印机，这个步骤可以忽略，付出的成本与收益不符**。

如果要用百分表测试，请注意以下内容：

- 百分表必须与要测试的垂直，不然有偏差。需要那个水平仪来测量。当然如果自己打印一个 3D 直角模型，挂在 x 轴上就更好了。
- 每次 1mm，移动 10 次或者 20 次进行测试，不要一次移动 10mm。
- 百分表要固定紧不，如果有松动都不准的。
- 用 M92 查看当前的步进数。

## 1、概要

- 当前 20 元的电机 1 个脉冲的行程 0.0125mm
- 影响理论精度达不到 的因素
  - 机加工误差
  - 机器装配的松紧
  - 打印中的震动
  - 同步轮
  - 皮带 弹性,平行度
  - 耗材特性/流动性

## 2、百分表校验

### 2.1 为啥要用百分表

测量 5cm 或者 2cm 的小方块，都不准确。这是因为打印部分是许多变量的结果，而不是 X、Y 和 Z 轴在打印期间移动的距离。

可以通过打印三个 20mm 校准立方体来进行简单的演示，无需更改机器，但每次测试都前改变挤出机流速。在下图中，立方体的流速为 80%、96% 和 120%。虽然它们从远处看是一样的，但用卡尺测量时，它们的外部尺寸有明显的差异。

![](./imgs/cubeflowvariation.jpg)

塑料在冷却时会收缩，这会因不同的材料甚至不同的颜色/年龄/耗材状况而有所不同。

### 2.2 测量原始轴运动

为了消除耗材的影响。因此，我们想在不打印时测量每个轴的运动，比较目标与实际运动。这是我们的卡尺或最好是百分表派上用场的地方。我们的目标是安装百分表，以便当我们移动轴时，它可以准确地测量它已经行进了多远。

#### ① 安装百分表

安装在 x 轴上，测量 x 的移动。

<img src="./imgs/qfb-y.jpg"  />

安装在热床头，测量 y 的移动距离

<img src="./imgs/qfb-yy.jpg"  />

安装 x 轴向下，测量 z 轴的情况

![](./imgs/qfb-z.jpg)

无论哪种情况，我们都必须遵守一些安装规则：

- 百分表必须牢固安装。如果它可以摆动或支架可以弯曲，则读数将不准确。
- 百分表的直线运动必须平行于被测轴的运动，或垂直于它所推动的物体。如果我们想象百分表与被测轴成 45 度角安装，我们可以看到读数仅为运动的一半。
- 安装到机器上测量 Z 轴行程时，请确保机器仍能安全归位，而百分表不会用完行程。如果无法做到这一点，请先将机器归位，然后安装百分表。

[Thingiverse 上的百分表安装座](https://www.thingiverse.com/thing:4803082)

#### ② 移动轴进行测量

- 先将机器归位，z 轴可以手动调整。
- 在测量之前，我们必须知道百分表的运动范围并进行相应的安装。如果百分表只能移动 25mm，那么要求移动 30mm 是没有意义的。这样做可能会在百分表触底时损坏百分表。
- 使用所选软件中的按钮将一个轴移动指定距离。10mm 通常是可以接受的，并且适合大多数百分表的运动范围。（100mm 实际上会更好，但超出了百分表的范围）。注意测量。使用反向按钮反转运动，并查看机器是否返回到百分表上的 0.00。
- 你也可以发出两个 10mm 的动作，看看有没有一致的错误。例如，如果机芯只有 9.95 毫米，您会期望第二个机芯降落在 19.90 毫米处，保持每 10 毫米 0.05 毫米的差异。

#### ③ 可容忍的误差

。考虑到使百分表完全垂直于行进方向是多么困难，这可能在可接受的误差范围内。如果你是手拿百分表进行测量，手的抖动产生的误差更大。

重要的是要记住这个距离有多小。10mm 移动上 0.05mm 的差异仅代表 0.5% 的误差。在许多情况下，这与打印的对象无关。

### 2.3 修正方案

#### ① 修改固件

```jsx
/**
 * inline: true
 */
import React from 'react';
import { XyzStep } from '3d-printer';
export default () => (
  <XyzStep
    xDefaultValue={80}
    yDefaultValue={80}
    zDefaultValue={400}
    xExpectValue={20}
    yExpectValue={20}
    zExpectValue={20}
    xActualValue={20}
    yActualValue={20}
    zActualValue={20}
  />
);
```

#### ② 切片软件中修正

正如我们从之前的 20 毫米校准立方体测试中所知道的，通过改变一些参数，可以影响整体的精度。

切片软件具有尺寸精度补偿

PrusaSlicer 在打印设置 > 高级 > 切片中找到）

![](./imgs/prusaslicercompensation.jpg)

Cura 中存在类似的功能（可在外壳 > 水平扩展中找到）：

![](./imgs/curacompensation.jpg)

需要对这些功能进行试验以充分了解它们的优缺点。例如，增加 X/Y 测量值可能会固定外部尺寸，但会对打印孔的精度产生负面影响。

#### ③ 升级机器硬件

改成线轨等等了。所以说入门机器就不要测试精度了。

有时可以升级机器以使其更准确。例如，有一个理论，即使用皮带轮而不是光滑表面的轴承作为皮带惰轮应该让皮带更一致地骑在惰轮上，因为皮带的齿在轴承表面上变形不均匀：

![](./imgs/bearingidlerdeformation.jpg)

#### ④ 修改设计文件

最后一种措施，也是最不理想的，是设计要打印更大或更小的部件以进行补偿。这是一种创可贴的方法，一旦我们打印出其他人设计的几何图形，它就会很快崩溃。

### 2.4、参考视频

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=848733846&bvid=BV1RL4y1B7Gu&cid=429591405&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

## 3、打印方块校准（不推荐）

用入门打印机，按照 40 的速度打印，大概需要 1 个多小时，要打印两个一共需要 2 个多小时。

### 3.1、打印 5cm 方块

打印一个[5mmTest.stl](/stl/5mmTest.stl)进行测试。

- 这个是自己设计的小方块，设置成有个空的盒子，这样打印完毕后，还可以再利用。不然浪费材料。

### 3.2、查出当前步进值

输入 M92 就可以了

```
M92
```

### 3.3、配置新的步进值

测量 5cm 的实际尺寸，并根据下表计算出实际的步进值。

```jsx
/**
 * inline: true
 */
import React from 'react';
import { XyzStep } from '3d-printer';
export default () => (
  <XyzStep
    xDefaultValue={80}
    yDefaultValue={80}
    zDefaultValue={400}
    xExpectValue={50}
    yExpectValue={50}
    zExpectValue={50}
    xActualValue={50}
    yActualValue={50}
    zActualValue={50}
  />
);
```
