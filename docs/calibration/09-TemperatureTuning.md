# 调试温度塔

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Warning } from '3d-printer';
export default () => {
  return <Warning />;
};
```

```jsx
/**
 * inline: true
 */
import React from 'react';
import { Desc } from '3d-printer';
export default () => (
  <Desc
    aim="为不同耗材设置喷嘴理想的温度"
    when="第一次组装、更换喷嘴或者不同型号的耗材时。"
    tools="本页面提供的打印文件"
    desc="测试需要40分钟，这个还是比较重要的。"
  />
);
```

**针对一些入门级的打印机，可以忽略这一步。**

对于这个校准，我们只关心热端的温度，而不是床。床温需要与任何给定的耗材相匹配，一旦找到合适的值，您通常会坚持使 用它。

<Alert type="info">
经验说明：

- 较高喷嘴温度的影响
  - 产生更结实的零件，尤其是层间粘合力。
  - 零件表面可能更亮。
  - 耗材会更柔软，因此渗出和拉丝可能会增加，并且可能会丢失一些表面细节，尤其是在悬垂处。
  - 热端温度过高可能会损坏组件的零件，例如内部 PTFE 管。
- 较低的喷嘴温度的影响
  - 导致较弱的零件，尤其是层间粘合力。
  - 零件表面可能较暗。
  - 耗材将更坚固，因此可以减少渗出和拉丝，并具有良好的表面细节，尤其是在悬垂处。
  - 会导致热端堵塞。
- 温度调整和回缩调整相互关联，两者的调整先后顺序没有关系，但是可能需要来回调整以达到理想结果。所以有人将这两个测试放在同一个温度塔进行。

</Alert>

## 1、校验方案

### 1.1 下载 stl 并切片

下载[temperaturetowerv2.stl](/stl/temperaturetowerv2.stl)软件，并使用 cure 进行切片。 下面默认的是用 0.5 喷嘴，0.25 层高进行切片。

### 1.2 设置每阶段温度

设置温度塔各个部分温度的方法有很多，可以通过`cura`后期脚本来生成，也可以自己写程序来实现，我就写了下面的一个程序。

可以用 cure 将[temperaturetowerv2.stl](/stl/temperaturetowerv2.stl)切片生成 gocode 文件，并上传到下面工具中来生成温度塔工具。如果你用的是 0.5mm 的喷头，按照 0.25 层高，可以按照下面默认的层数来设置温度。

```jsx
/**
 * inline: true
 */
import React from 'react';
import { TempratureTower } from '3d-printer';
export default () => <TempratureTower />;
```

> 今后优化的内容
>
> - 追加回抽，避免每个阶段因为温度调整，造成拖丝。
> - 按照打印到多高 mm 来设置温度，而不是按照层数设置温度，这样就不用管不同层高切出的模型了。
> - 可以修改除了 cura 以外切片软件的 gcode 文件。

### 1.3 判断适合的温度

看看那个部分最丝滑，然后用手掰一下，看看粘连的程度。然后选择一个最合适的温度。

## 2、其他校验方案

如果你觉得上面的校验方案不满意，可以使用其他方案来做温度塔实验。 现在都是使用其他人的模型来做温度塔，其实如果今后有时间的话，自己可以设置模型来测试温度塔。

### 2.1 使用 cura 设置温度

- 在后期脚本中进行设置。
  - 添加一个脚本：changeAtz
  - 选择层号
  - 选择从第几层开始，后面的层都继承
  - 选择修改挤出机的温度，并指定温度。

<img src="./imgs/temp-town-addscript.jpg" style="zoom:50%;" />

生成的 gecode 可以跟没有添加脚本的做对比。

<img src="./imgs/temp-twon-company.jpg" style="zoom:50%;" />

使用 cura 的问题：

- 添加比较麻烦，次数过多。
- 后期处理脚本，在每个模型都使用，如果要使用新模型，就要删除。
- 不能添加自己的其他脚本。例如回抽。

### 2.2 使用 calibrition shape 模型

添加模型

<img src="./imgs/calibration-shapes-temptower1.jpg" style="zoom:67%;" />

出现的型状如下：

<img src="./imgs/calibration-shapes-temptower2.jpg" style="zoom:50%;" />

然后使用 2.1 进行温度设置。

## 3、温度塔原理

以下表格将创建一个温度塔，有五个段可以改变温度。通常，最低温度将在打印开始时（A 段），而在打印顶部（E 段）升高到最高温度。

设置最高温度与最低温度，不能超过打印机可以承受的最低热端温度和最高热端温度，以确保安全。

这里有一个复杂的模型，STL：[temperaturetowerv2.stl](/stl/temperaturetowerv2.stl)。打印时间更短，悬垂变化更多，每个子阶段都有一个窄金字塔，可以尝试折断以测试层附着力。

<img src="./imgs/temperatureresults.jpg" style="zoom:67%;" />

### 3.1 如何找到间隔层？

在 cura 中可以看到切片过程。这里假设按照 0.25 进行切片。一共切了 163 层。找到如下的规律：

- 每个阶段的最后一层是一个平铺层。只有一层。
- 每个阶段的开始层是一个平铺层，这个平铺层有一个小方块。
- 每个阶段间隔了 32 层。
  - A 层从 4 层开始 ：190°
  - B 层从 36 层开始 ：195°
  - C 层从 68 层开始 ：200°
  - D 层从 100 层开始 ：205°
  - E 层从 132 层开始 ：210°
  - E 层从 163 层结束

<img src="./imgs/temp-town-firstlayer.jpg" style="zoom:33%;" />

<img src="./imgs/temp-town-Blayer.jpg" style="zoom: 33%;" />

### 3.2 其他工具的做法

可以在[这个网站](https://teachingtechyt.github.io/calibration.html#temp)导出 gcode，并观察出 gcode 的规律。

每层结束前做了回抽，然后又重置了回抽

```
G1 E-5 F2400 ; custom retraction
;layer 200, Z = 40.000
;fan200;
G1 Z40.000 F1200
G1 X86.500 Y58.500 F6000
G1 E0 F2400 ; custom un-retraction/prime
```

通过`M104 S196`设置温度，每层温度偏差是 5 度。

```
G92 E0.0000
G1 E-5 F2400 ; custom retraction
M104 S191 T0 ; custom hot end temp - A
M106 S255; custom fan 100% from layer 2
G1 Z0.400 F1200
G1 X86.379 Y56.500 F6000
G1 E0 F2400 ; custom un-retraction/prime
G92 E0.0000

G92 E0.0000
G1 E-5 F2400 ; custom retraction
M104 S196 T0 ; custom hot end temp - B
;layer 46, Z = 9.200
;fan46;
G1 Z9.200 F1200
G1 X86.100 Y58.900 F6000
G1 E0 F2400 ; custom un-retraction/prime
G92 E0.0000
```

## 4、参考视频

视频

- 3d 打印校准 1 打印温度塔最简单的温度塔打印教程
  - 看是否可以轻松掰开，如果可以就将风扇调低，这样会导致温度上升，提高粘合度。

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=854608198&bvid=BV1wL4y1K7aL&cid=735274372&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```

<br/><br/>

- 3D 打印新手必学技能：调平、温度塔、回抽塔、打印质量测试、精度校准等

```jsx
/**
 * inline: true
 */
import React from 'react';
import { VideoCard } from '3d-printer';
export default () => {
  return (
    <VideoCard>
      <iframe
        src="//player.bilibili.com/player.html?aid=602159915&bvid=BV1jB4y1V7Ss&cid=807945500&page=1"
        scrolling="no"
        border="0"
        frameborder="no"
        framespacing="0"
        allowfullscreen="true"
      >
        {' '}
      </iframe>
    </VideoCard>
  );
};
```
